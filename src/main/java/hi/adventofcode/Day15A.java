package hi.adventofcode;

import hi.utils.assets.vis.FrameStore;
import hi.utils.collection.ArrayOrderedSet;
import hi.utils.gui.components.HIImage;
import hi.utils.tuple.Tuple2;

import java.awt.Dimension;
import java.awt.KeyboardFocusManager;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.function.LongConsumer;
import java.util.function.LongSupplier;

import javax.swing.JFrame;


public class Day15A {
	
	public static final int
			NORTH = 1,
			EAST = 4,
			SOUTH = 2,
			WEST = 3;
	
	public static final int
			NO_MOVES_LEFT = -1,
			MOVE_FAILED = 0,
			MOVE_SUCCESS = 1,
			FOUND_OXYGEN = 2;
	
	public enum Tile {
		UNKNOWN, WALL, SPACE, OXYGEN
	}
		
	private static Set<Point> neighbours(Point p) {
		Set<Point> points = new ArrayOrderedSet<>(4);
		points.add(new Point(p.x-1, p.y));
		points.add(new Point(p.x, p.y-1));
		points.add(new Point(p.x+1, p.y));
		points.add(new Point(p.x, p.y+1));
		return points;
	}
	
	public static class Droid implements LongSupplier, LongConsumer {
		
		private final BlockingQueue<Long> outbound = new ArrayBlockingQueue<>(1);
		private final BlockingQueue<Long> inbound = new ArrayBlockingQueue<>(1);
		private int x, y;

		public Droid(int x, int y) {
			this.x = x;
			this.y = y;
		}

		@Override
		public long getAsLong() {
			try {
				return inbound.take();
			} catch(InterruptedException ex) {
				return 0;
			}
		}

		@Override
		public void accept(long value) {
			try {
				outbound.put(value);
			} catch(InterruptedException ex) {}
		}
		
		public int perfromAction(int action) throws InterruptedException {
			inbound.put((long)action);
			int result = outbound.take().intValue();
			if(result==MOVE_SUCCESS || result == FOUND_OXYGEN) {
				switch(action) {
					case NORTH: y--; break;
					case EAST:  x++; break;
					case SOUTH: y++; break;
					case WEST:  x--; break;
				}
			}
			return result;
		}
		
		public int mapAction(Tile[][] map, int action) throws InterruptedException {
				int droidPrevX = x;
				int droidPrevY = y;
				int result = perfromAction(action);
				switch(result) {
					case MOVE_FAILED:
						int wallX = x;
						int wallY = y;
						switch(action) {
							case NORTH: wallY--; break;
							case EAST:  wallX++; break;
							case SOUTH: wallY++; break;
							case WEST:  wallX--; break;
						}
						map[wallX][wallY] = Tile.WALL;
						break;
					case MOVE_SUCCESS: {
						map[x][y] = Tile.SPACE;
						break;
					}
					case FOUND_OXYGEN: {
						map[x][y] = Tile.OXYGEN;
						//TODO: A* from droid to initialX, initialY
						break;
					}
				}
				return result;
		}
		
		public int autopilot(Tile[][] map) throws InterruptedException {
			//scan for unknown
			Queue<Tuple2<Integer,Point>> toScan = new LinkedList<>();
			Set<Point> dejavu = new HashSet<>();
			
			toScan.add(new Tuple2<>(NORTH, new Point(x,y-1)));
			toScan.add(new Tuple2<>(EAST,  new Point(x+1,y)));
			toScan.add(new Tuple2<>(SOUTH, new Point(x,y+1)));
			toScan.add(new Tuple2<>(WEST,  new Point(x-1,y)));
			dejavu.add(new Point(x,y));
			
			while(!toScan.isEmpty()) {
				
				Tuple2<Integer,Point> p = toScan.poll();
				if(dejavu.contains(p.get2())) continue;
				dejavu.add(p.get2());
//				System.out.printf("Scanning %d,%d\n", p.get2().x, p.get2().y);
				
				switch(map[p.get2().x][p.get2().y]) {
					case WALL:
						continue;
					case SPACE: case OXYGEN:
						for(Point n : neighbours(p.get2()))
							toScan.add(new Tuple2<>(p.get1(), n));
						break;
					case UNKNOWN:
						//move towards unknown
						return mapAction(map,p.get1());
				}
			}
			//no reachable unexplored spaces, move not possible
			return NO_MOVES_LEFT;
		}
		
	}
	
	private static final int DROID_COLOR  = 0xFF0000;
	private static final int WALL_COLOR   = 0x1F1F1F;
	private static final int SPACE_COLOR  = 0x7F7F7F;
	private static final int OXYGEN_COLOR = 0x3F3FFF;
	
	private static void paintMap(BufferedImage dest, Tile[][] map, Droid droid) {
		for(int x=0; x<MAP_WIDTH; x++)
			for(int y=0; y<MAP_HEIGHT; y++) {
				int color = 0;
				switch(map[x][y]) {
					case SPACE:  color = SPACE_COLOR; break;
					case WALL:   color = WALL_COLOR; break;
					case OXYGEN: color = OXYGEN_COLOR; break;
				}
				dest.setRGB(x, y, color);
			}
		dest.setRGB(droid.x, droid.y, DROID_COLOR);
	}
	
	public static void findShortestPath(final Point inital, final Point dest, Tile[][] map, BufferedImage img, HIImage tiles) {
		Set<Point> dejavu = new HashSet<>();
		
		class Path {
			final Point point;
			final Path prev;
			final int knownCost;
			final int estimatedCost;

			public Path(Point point) { this(point, null); }
			public Path(Point point, Path prev) {
				this.point = point;
				this.prev = prev;
				knownCost = prev==null ? 0 : prev.knownCost+1;
				estimatedCost = Math.abs(dest.x-point.x) + Math.abs(dest.y-point.y);
			}
			
			public int getCost() { return knownCost+estimatedCost; }
		}
		
		Queue<Path> q = new PriorityQueue<>(Comparator.comparingInt(Path::getCost));
		q.add(new Path(inital));
		
		while(!q.isEmpty()) {
			Path p = q.poll();
			if(dejavu.contains(p.point)) continue;
			dejavu.add(p.point);
			
			img.setRGB(p.point.x, p.point.y, 0xFFFFFF);
			tiles.repaint();
			
			if(p.point.equals(dest)) {
				System.out.println(p.getCost());
				return;
			}
			
			for(Point pt : neighbours(p.point)) {
				if(map[pt.x][pt.y]==Tile.SPACE || map[pt.x][pt.y]==Tile.OXYGEN)
					q.add(new Path(pt, p));
			}
		}
		
		//no path
		System.out.println("No path");
	}
	
	private static final int MAP_WIDTH  = 44;
	private static final int MAP_HEIGHT = 44;
	
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		
		Tile[][] map = new Tile[MAP_WIDTH][MAP_HEIGHT];
		final int initialX = MAP_WIDTH/2;
		final int initialY = MAP_HEIGHT/2;
		map[initialX][initialY] = Tile.SPACE;
		for(Tile[] row : map) Arrays.fill(row, Tile.UNKNOWN);
		Droid droid = new Droid(initialX,initialY);
		Intcode comp = new Intcode(INPUT, droid, droid);
		Intcode.run(comp);
		
		BufferedImage img = new BufferedImage(MAP_WIDTH, MAP_HEIGHT, BufferedImage.TYPE_INT_RGB);
		img.setRGB(initialX, initialY, DROID_COLOR);
		HIImage tiles = new HIImage(new FrameStore("Screen", img), true, false, true);
		tiles.setInterpolate(RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR);
		
		KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(e -> {
			if(e.getID()==KeyEvent.KEY_PRESSED) {
				int action=0;
				switch(e.getKeyCode()) {
					case KeyEvent.VK_UP:    action=NORTH; break;
					case KeyEvent.VK_DOWN:  action=SOUTH; break;
					case KeyEvent.VK_LEFT:  action=WEST;  break;
					case KeyEvent.VK_RIGHT: action=EAST;  break;
				}
				if(action==0) return false;
				try {
					droid.mapAction(map, action);
					paintMap(img, map, droid);
					tiles.repaint();
				} catch(InterruptedException ex) {}
			}
			return false;
		});
		
		tiles.setPreferredSize(new Dimension(MAP_WIDTH*8, MAP_HEIGHT*8));
		JFrame frame = new JFrame("Map");
		frame.add(tiles);
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
		try {
			int result;
			while( (result=droid.autopilot(map))!=NO_MOVES_LEFT) {
				if(result == FOUND_OXYGEN) {
					findShortestPath(new Point(initialX, initialY), new Point(droid.x, droid.y), map, img, tiles);
					break;
				}
			}
			paintMap(img, map, droid);
			tiles.repaint();
		} catch(InterruptedException ex) {
		}
		
		System.out.printf("%dms\n", System.currentTimeMillis()-start);
	}
	
	public static long[] INPUT = {3,1033,1008,1033,1,1032,1005,1032,31,1008,1033,2,1032,1005,1032,58,1008,1033,3,1032,1005,1032,81,1008,1033,4,1032,1005,1032,104,99,101,0,1034,1039,101,0,1036,1041,1001,1035,-1,1040,1008,1038,0,1043,102,-1,1043,1032,1,1037,1032,1042,1105,1,124,101,0,1034,1039,102,1,1036,1041,1001,1035,1,1040,1008,1038,0,1043,1,1037,1038,1042,1106,0,124,1001,1034,-1,1039,1008,1036,0,1041,102,1,1035,1040,1001,1038,0,1043,1001,1037,0,1042,1106,0,124,1001,1034,1,1039,1008,1036,0,1041,1001,1035,0,1040,1001,1038,0,1043,1002,1037,1,1042,1006,1039,217,1006,1040,217,1008,1039,40,1032,1005,1032,217,1008,1040,40,1032,1005,1032,217,1008,1039,7,1032,1006,1032,165,1008,1040,5,1032,1006,1032,165,1102,1,2,1044,1105,1,224,2,1041,1043,1032,1006,1032,179,1101,0,1,1044,1105,1,224,1,1041,1043,1032,1006,1032,217,1,1042,1043,1032,1001,1032,-1,1032,1002,1032,39,1032,1,1032,1039,1032,101,-1,1032,1032,101,252,1032,211,1007,0,27,1044,1106,0,224,1102,1,0,1044,1106,0,224,1006,1044,247,101,0,1039,1034,101,0,1040,1035,102,1,1041,1036,1001,1043,0,1038,102,1,1042,1037,4,1044,1106,0,0,13,3,18,86,2,10,5,16,95,16,54,4,23,63,70,10,21,20,26,99,85,9,96,3,83,5,9,91,14,1,4,78,11,15,53,10,35,13,7,17,30,90,23,65,65,67,16,4,65,39,11,57,13,36,22,95,53,63,22,47,12,47,2,12,3,71,92,17,55,16,51,79,6,3,92,15,17,15,18,63,8,12,3,49,6,69,32,1,25,83,17,12,1,76,23,95,17,13,92,13,56,16,69,94,11,20,31,83,30,21,88,22,61,45,6,70,12,3,30,23,86,6,93,4,24,9,73,72,7,72,83,9,30,6,24,86,99,11,11,96,16,68,10,35,19,23,6,79,51,8,3,8,75,2,32,26,73,23,80,30,86,25,64,46,24,81,20,18,85,7,94,28,37,93,18,12,77,99,14,22,19,50,2,18,45,63,8,2,89,79,79,7,33,77,18,20,22,12,58,61,20,4,58,20,51,79,14,32,19,87,21,19,76,8,81,7,13,72,75,22,28,22,14,92,30,18,90,10,6,97,25,34,9,20,26,52,45,6,4,97,4,46,26,86,61,20,25,28,26,22,54,69,16,51,3,58,5,23,75,92,18,98,12,11,55,38,22,87,14,20,17,52,73,9,91,30,14,26,12,56,81,54,9,72,18,12,47,93,22,54,21,59,73,7,78,12,87,26,5,39,45,4,55,16,21,86,62,20,98,61,14,20,70,14,25,92,32,44,2,3,15,32,23,23,97,76,78,15,23,95,21,11,69,34,12,89,3,95,24,15,59,38,39,72,14,15,55,48,18,2,43,26,13,58,68,11,22,89,33,79,22,43,40,14,26,5,50,11,28,9,36,33,2,22,43,21,90,15,92,14,14,49,9,80,14,85,99,70,8,16,14,15,70,1,39,32,45,5,57,12,12,4,99,75,28,14,2,28,71,5,69,61,4,28,98,97,87,10,80,2,65,93,6,21,81,7,95,22,35,18,38,23,11,53,14,5,2,84,3,70,33,19,8,52,10,99,14,58,36,1,3,30,53,4,7,47,10,93,2,32,17,40,68,43,20,41,4,16,21,29,23,82,2,18,37,37,15,19,26,41,28,9,95,17,17,52,25,13,49,28,47,22,5,52,14,21,72,83,7,17,86,20,3,18,58,14,19,25,56,65,65,26,53,8,20,75,31,21,40,17,6,33,20,95,47,24,75,26,17,96,24,48,65,97,4,52,20,78,47,14,23,77,32,8,18,98,43,7,61,25,84,40,6,36,24,87,24,71,77,13,20,49,16,60,35,9,64,48,21,2,74,25,1,2,57,11,58,7,45,35,26,13,74,92,2,9,82,9,20,23,15,33,94,7,10,48,78,16,24,94,33,11,21,5,89,47,15,52,12,51,51,81,9,18,39,14,2,97,79,33,23,12,99,3,16,11,79,83,45,18,23,78,86,69,10,25,98,62,62,18,7,44,47,1,3,92,8,22,81,9,3,29,8,81,21,13,95,6,5,99,5,29,16,3,53,72,26,14,44,97,7,43,12,42,65,17,8,12,88,55,18,20,34,13,39,10,72,58,15,11,69,17,94,20,22,52,28,13,30,65,8,2,63,18,4,36,17,8,71,16,71,15,64,14,31,51,75,1,12,92,14,35,23,40,45,1,5,87,28,18,83,43,9,90,2,3,50,18,61,68,5,89,16,44,7,34,82,74,15,83,15,70,13,80,20,43,8,35,14,58,50,75,20,50,9,68,46,52,2,73,11,60,32,61,25,40,9,31,21,73,0,0,21,21,1,10,1,0,0,0,0,0,0};

}
