package hi.adventofcode;



import hi.utils.tuple.Tuple2;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Day10A2 {
	
	public static int gcd(int x, int y) {
		if(x<0) x=-x;
		if(y<0) y=-y;
		if(y==0) return x;
		if(x==1 || y==1) return 1;
		return gcd_internal(y, x%y);
	}
	
	private static int gcd_internal(int x, int y) {
		if(y==0) return x;
		return gcd_internal(y,x%y);
	}
	
	public static class Belt {
		
		private final int width, height;
		private final boolean[][] map;
		private final List<Point> asteroids = new ArrayList<>();

		public Belt(boolean[][] map) {
			this.height = map.length;
			this.width = map[0].length;
			this.map = map;
			for(int y=0; y<height; y++)
				for(int x=0; x<width; x++)
					if(map[y][x])
						asteroids.add(new Point(x,y));
		}
		
		public int countVisible(Point asteroid) {
			int count=0;
			for(int x=0; x<width; x++) {
				for(int y=0; y<height; y++) {
					int dx = x - asteroid.x;
					int dy = y - asteroid.y;
					if(gcd(dx,dy)!=1 || (dx==0 && dy==0)) continue;
					
					int cx=x, cy=y;
					do {
						if(map[cy][cx]) {
							count++;
							break;
						}
						cx+=dx;
						cy+=dy;
					} while(cx>=0 && cx<width && cy>=0 && cy<height);
				}
			}
			return count;
		}
		
		public List<Point> getVisible(Point asteroid) {
			List<Point> points = new ArrayList<>(230);
			for(int x=0; x<width; x++) {
				for(int y=0; y<height; y++) {
					int dx = x - asteroid.x;
					int dy = y - asteroid.y;
					if(gcd(dx,dy)!=1 || (dx==0 && dy==0)) continue;
					
					int cx=x, cy=y;
					do {
						if(map[cy][cx]) {
							points.add(new Point(cx,cy));
							break;
						}
						cx+=dx;
						cy+=dy;
					} while(cx>=0 && cx<width && cy>=0 && cy<height);
				}
			}
			return points;
		}
	}
	
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		
		Belt belt = new Belt(Day10A.INPUT.lines()
				.map(l -> {
					boolean[] bs = new boolean[l.length()];
					for(int i=0; i<bs.length; i++)
						bs[i] = l.charAt(i)=='#';
					return bs;
				})
				.toArray(boolean[][]::new));
		
//		System.out.println(belt.countVisible(new Point(0,0)));
		
//		for(Point p : belt.asteroids) {
//			System.out.printf("(%d,%d) => %d\n", p.x, p.y, belt.countVisible(p));
//		}
		
		System.out.println(belt.asteroids.stream()
				.map(p -> new Tuple2<>(p, belt.countVisible(p)))
				.max(Comparator.comparing(tpl->tpl.get2()))
				.get());
		
//		System.out.println(belt.asteroids.stream()
//				.mapToInt(belt::countVisible)
//				.max()
//				.getAsInt());
		
		
		System.out.printf("%dms\n", System.currentTimeMillis()-start);
	}
	
	public static String TEST =
"#.........\n" +
"...A......\n" +
"...B..a...\n" +
".EDCG....#\n" +
"..F.c.b...\n" +
".....c....\n" +
"..efd.c.##\n" +
".......c..\n" +
"....#...c.\n" +
"...#..#..#";

}
