package hi.adventofcode;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day22B {
	
	private static final Pattern PARSER_PATTERN = Pattern.compile("cut (-?\\d+)|deal into new stack|deal with increment (\\d+)");
	
	public static interface Shuffle {
		public static Shuffle parse(String line) {
			Matcher m = PARSER_PATTERN.matcher(line);
			if(!m.matches()) throw new IllegalArgumentException();
			
			if(m.start(1)>=0) return new Cut(Integer.parseInt(m.group(1)));
			if(m.start(2)>=0) return new Increment(Integer.parseInt(m.group(2)));
			return new Stack();
		}
		
		long movesTo(long position, long deckLength);
	}
	
	public static class Stack implements Shuffle {
		@Override public long movesTo(long position, long deckLength) { return deckLength-(position+1); }
	}
	
	public static class Cut implements Shuffle {
		private final int position;
		public Cut(int position) { this.position = position; }
		@Override public long movesTo(long position, long deckLength) {
			long cut = this.position>0 ? this.position : deckLength+this.position;
			if(position<cut) return position+(deckLength-cut);
			return position-cut;
		}
	}
	
	public static class Increment implements Shuffle {
		private final int interval;
		public Increment(int interval) { this.interval = interval; }
		@Override public long movesTo(long position, long deckLength) { return (position*interval)%deckLength; }
	}
	
	public static void main(String[] args) {

		Shuffle[]  actions = Day22A.INPUT.lines()
//				.limit(4)
//				.peek(System.out::println)
				.map(Shuffle::parse)
				.toArray(Shuffle[]::new);
		final long deckLength = 119315717514047L;
		final long target = 2020;
//		long repeats = 101741582076661L;
		long repeats = 10000000L;
		
		long start = System.currentTimeMillis();
		
		long position = target;
		while(repeats-->0) for(Shuffle action : actions)
			position = action.movesTo(position, deckLength);
		System.out.println(position);
		
		
		System.out.printf("%dms\n", System.currentTimeMillis()-start);
	}

}
