package hi.adventofcode;


import hi.utils.math.SoftMath;
import hi.utils.tuple.Tuple2;

import java.awt.Point;
import java.util.Comparator;

public class Day10B {
	
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		
		Point station = new Point(11,11);
		int[] index = new int[]{1};
		
		new Day10A2.Belt(Day10A.INPUT.lines()
				.map(l -> {
					boolean[] bs = new boolean[l.length()];
					for(int i=0; i<bs.length; i++)
						bs[i] = l.charAt(i)=='#';
					return bs;
				})
				.toArray(boolean[][]::new))
				.getVisible(station)
				.stream()
				.map(p -> {
					int dx = p.x-station.x;
					int dy = p.y-station.y;
					double angle = Math.atan2(dx, -dy);
					return new Tuple2<>(p, angle<0?angle+SoftMath.TWO_PI:angle);
				})
				.sorted(Comparator.comparingDouble(tpl->tpl.get2()))
				.map(tpl2 -> tpl2.merge(index[0]++))
				.skip(195)
				.limit(10)
				.forEachOrdered(System.out::println);
		
		
		
		System.out.printf("%dms\n", System.currentTimeMillis()-start);
	}
	
}
