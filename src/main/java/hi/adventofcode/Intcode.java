package hi.adventofcode;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.LongConsumer;
import java.util.function.LongSupplier;

public class Intcode implements Runnable {
	
	public static void runAndWait(Intcode ... computers) {
		if(computers.length==1) {
			computers[0].run();
		} else {
			ExecutorService threadPool = Executors.newFixedThreadPool(computers.length-1);
			List<Future<?>> futures = new ArrayList<>(computers.length-1);
			for(int i=1; i<computers.length; i++)
				futures.add(threadPool.submit(computers[i]));
			threadPool.shutdown();
			computers[0].run();
			for(Future<?> f : futures) {
				try {
					f.get();
				} catch(InterruptedException ex) {
					ex.printStackTrace();
				} catch(ExecutionException ex) {
					ex.getCause().printStackTrace();
				}
			}
		}
	}
	
	public static void run(Intcode ... computers) {
		ExecutorService threadPool = Executors.newFixedThreadPool(computers.length);
		for(Intcode computer : computers) threadPool.submit(computer);
		threadPool.shutdown();
	}
	
	public static Pipe connect(Intcode output, Intcode input) throws IOException {
		Pipe pipe = new Pipe();
		output.setOut(pipe);
		input.setIn(pipe);
		return pipe;
	}
	
	public static class SequenceSupplier implements LongSupplier {
		
		private final long[] sequence;
		private int index;

		public SequenceSupplier(long ... sequence) {
			this.sequence = sequence;
			index = 0;
		}
		@Override public long getAsLong() { return sequence[index++]; }
	}
	
	public static class PrefixSupplier implements LongSupplier {
		
		private final long[] sequence;
		private int index;
		private final LongSupplier next;

		public PrefixSupplier(long[] sequence, LongSupplier next) {
			this.sequence = sequence;
			index = 0;
			this.next = next;
		}
		@Override public long getAsLong() { return index<sequence.length ? sequence[index++] : next.getAsLong(); }
	}
	
	public static class Pipe implements LongConsumer, LongSupplier {
		
		private final BlockingQueue<Long> internal = new ArrayBlockingQueue<>(2);
		private final String name;
		private boolean dead = false;

		public Pipe() { this(null); }
		public Pipe(String name) { this.name = name; }

		@Override
		public void accept(long value) {
			if(dead) return;
			try {
				if(name!=null) System.out.printf("%s pushing %d\n", name, value);
				internal.put(value);
				if(name!=null) System.out.printf("%s pushed\n", name);
			} catch(InterruptedException ex) {}
		}

		@Override
		public long getAsLong() {
			if(dead) {
				Long value = internal.poll();
				return value==null ? 0 : value;
			};
			try {
				if(name!=null) System.out.printf("%s waiting\n", name);
				long value = internal.take();
				if(name!=null) System.out.printf("%s pulled %d\n", name, value);
				return value;
			} catch(InterruptedException ex) {
				throw new IllegalStateException(ex);
			}
		}
	}
	
	public static class AsciiInput implements LongSupplier {
		
		private final Reader reader;

		public AsciiInput(Reader reader) { this.reader = reader; }
		public AsciiInput(String string) { this.reader = new StringReader(string); }

		@Override
		public long getAsLong() {
			try {
				return reader.read();
			} catch (IOException ex) {
				throw new IllegalStateException(ex);
			}
		}
		
	}
	
	public static class AsciiOutput implements LongConsumer {

		@Override
		public void accept(long value) {
			if(value>255)
				System.out.println(value);
			else
				System.out.print((char)value);
		}
		
	}
	
	public static final int
			ADD  =  1,
			MUL  =  2,
			IN   =  3,
			OUT  =  4,
			JNZ  =  5,
			JZ   =  6,
			LT   =  7,
			EQ   =  8,
			BASE =  9,
			HALT = 99;
	
	public static final int
			POSITION = 0,
			IMMEDIATE = 1,
			RELATIVE = 2;
	
	private long[] memory;
	private int progamCounter = 0;
	private long base = 0;
	
	private LongSupplier in;
	private LongConsumer out;
	private boolean terminated = false;
	private Runnable onTerminate = null;

	public Intcode(long[] program) { this.memory = program; setIn(System.in); setOut(System.out); }
	public Intcode(long[] program, LongSupplier in, LongConsumer out) { this.memory = program; setIn(in); setOut(out); }

	@Override public void run() { while(step()) {} }
	
	@SuppressWarnings("SleepWhileInLoop")
	public void run(long interval) { 
		while(step()) {
			try {
				Thread.sleep(interval);
			} catch(InterruptedException ex) {}
		}
	}
	
	public long[] getMemory() { return memory; }
	public long getProgamCounter() { return progamCounter; }
	public void setProgamCounter(int progamCounter) { this.progamCounter = progamCounter; }

	public LongSupplier getIn() { return in; }
	public void setIn(LongSupplier in) { this.in = in; }
	public void setIn(InputStream in) {
		final BufferedReader br = new BufferedReader(new InputStreamReader(in));
		this.in = () -> {
			try {
				return Integer.parseInt(br.readLine());
			} catch(IOException ex) {
				return 0;
			}
		};
	}
	public void setIn(long ... sequence) {
		this.in = new LongSupplier() {
			int index = 0;
			@Override public long getAsLong() { return sequence[index++]; }
		};
	}
	public LongConsumer getOut() { return out; }
	public void setOut(LongConsumer out) { this.out = out; }
	public void setOut(OutputStream out) { setOut(new PrintStream(out)); }
	public void setOut(PrintStream out) {this.out = out::println; }
	public boolean isTerminated() { return terminated; }
	public Runnable getOnTerminate() { return onTerminate; }
	public void setOnTerminate(Runnable onTerminate) { this.onTerminate = onTerminate; }
	
//	private long get(long addr, int parameterMode) { return get((int)addr, parameterMode);}
	private long get(long addr, int parameterMode) {
		switch(parameterMode) {
			case POSITION: return addr>memory.length ? 0 : memory[(int)addr];
			case IMMEDIATE: return addr;
			case RELATIVE: return base+addr>memory.length ? 0 : memory[(int)(base+addr)];
			default: throw new UnsupportedOperationException();
		}
	}
	
	private void set(int addr, long value) {
		if(addr>=memory.length) memory = Arrays.copyOf(memory, addr*2);
		memory[addr]=value;
	}
	
	private void set(long addr, int parameterMode, long value) {
		if(addr<0) throw new IndexOutOfBoundsException();
		switch(parameterMode) {
			case POSITION:
				set((int)get(addr, POSITION), value);
				return;
				
			case IMMEDIATE:
				set((int)addr, value);
				return;
				
			case RELATIVE:
				set((int)(get(addr, POSITION)+base), value);
				return;
			default: throw new UnsupportedOperationException();
		}
	}
	
	public void sendOutputTo(Intcode computer) throws IOException {
		connect(this, computer);
	}

	@SuppressWarnings("UseSpecificCatch")
	public boolean step() {
		try {
			int instruction = (int)memory[progamCounter];
			int opcode = instruction%100;
			instruction/=100;
			int modeA = instruction%10;
			instruction/=10;
			int modeB = instruction%10;
			int modeC = instruction/10;
			switch(opcode) {
				case ADD: {
					progamCounter++;
					long a = get(memory[progamCounter++], modeA);
					long b = get(memory[progamCounter++], modeB);
					set(progamCounter++, modeC, a+b);
					return true;
				}
				case MUL: {
					progamCounter++;
					long a = get(memory[progamCounter++], modeA);
					long b = get(memory[progamCounter++], modeB);
					set(progamCounter++, modeC, a*b);
					return true;
				}
				case IN: {
					progamCounter++;
					set(progamCounter++,modeA,in.getAsLong());
					return true;
				}
				case OUT: {
					progamCounter++;
					out.accept(get(memory[progamCounter++], modeA));
					return true;
				}
				case JNZ: {
					progamCounter++;
					long cond = get(memory[progamCounter++],modeA);
					long lbl = get(memory[progamCounter],modeB);
					if(cond!=0) {
						progamCounter = (int)lbl;
					} else {
						progamCounter++;
					}
					return true;
				}
				case JZ: {
					progamCounter++;
					long cond = get(memory[progamCounter++],modeA);
					long lbl = get(memory[progamCounter],modeB);
					if(cond==0) {
						progamCounter = (int)lbl;
					} else {
						progamCounter++;
					}
					return true;
				}
				case LT: {
					progamCounter++;
					long a = get(memory[progamCounter++],modeA);
					long b = get(memory[progamCounter++],modeB);
					set(progamCounter++, modeC, a<b?1:0);
					return true;
				}
				case EQ: {
					progamCounter++;
					long a = get(memory[progamCounter++],modeA);
					long b = get(memory[progamCounter++],modeB);
					set(progamCounter++, modeC, a==b?1:0);
					return true;
				}
				case BASE: {
					progamCounter++;
					base += get(memory[progamCounter++],modeA);
					return true;
				}
				case HALT: {
					terminated = true;
					if(onTerminate!=null) onTerminate.run();
					return false;
				}
				default: throw new UnsupportedOperationException(String.format("%2d(%d,%d,%d)", opcode, modeA, modeB, modeC));
			}
		} catch (Throwable ex) {
			ex.printStackTrace();
			terminated = true;
			if(onTerminate!=null) onTerminate.run();
			return false;
		}
	}
	
}
