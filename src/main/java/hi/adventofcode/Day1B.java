package hi.adventofcode;

import java.util.function.IntPredicate;
import java.util.function.IntUnaryOperator;
import java.util.stream.IntStream;

public class Day1B {
	
	private static final IntUnaryOperator FUEL = m->m/3-2;
	private static final IntPredicate POSITIVE = f->f>0;
	
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		System.out.println(INPUT.lines().mapToInt(Integer::parseInt).map(FUEL).flatMap(f->IntStream.iterate(f, POSITIVE, FUEL)).sum());
		System.out.printf("%dms\n", System.currentTimeMillis()-start);
	}
	
	private static final String INPUT = "83326\n" +
"84939\n" +
"135378\n" +
"105431\n" +
"119144\n" +
"124375\n" +
"138528\n" +
"88896\n" +
"98948\n" +
"85072\n" +
"112576\n" +
"144497\n" +
"112824\n" +
"98892\n" +
"81551\n" +
"139462\n" +
"73213\n" +
"93261\n" +
"130376\n" +
"118425\n" +
"132905\n" +
"54627\n" +
"134676\n" +
"140435\n" +
"131410\n" +
"128441\n" +
"96755\n" +
"94866\n" +
"89490\n" +
"122118\n" +
"106596\n" +
"77531\n" +
"84941\n" +
"57494\n" +
"97518\n" +
"136224\n" +
"69247\n" +
"147209\n" +
"92814\n" +
"63436\n" +
"79819\n" +
"109335\n" +
"85698\n" +
"110103\n" +
"79072\n" +
"52282\n" +
"73957\n" +
"68668\n" +
"105394\n" +
"149663\n" +
"91954\n" +
"66479\n" +
"55778\n" +
"126377\n" +
"75471\n" +
"75662\n" +
"71910\n" +
"113031\n" +
"133917\n" +
"76043\n" +
"65086\n" +
"117882\n" +
"134854\n" +
"60690\n" +
"67495\n" +
"62434\n" +
"67758\n" +
"95329\n" +
"123078\n" +
"128541\n" +
"108213\n" +
"93543\n" +
"147937\n" +
"148262\n" +
"56212\n" +
"148586\n" +
"73733\n" +
"110763\n" +
"149243\n" +
"133232\n" +
"95817\n" +
"68261\n" +
"123872\n" +
"93764\n" +
"147297\n" +
"51555\n" +
"110576\n" +
"89485\n" +
"109570\n" +
"88052\n" +
"132786\n" +
"70585\n" +
"105973\n" +
"85898\n" +
"149990\n" +
"114463\n" +
"147536\n" +
"67786\n" +
"139193\n" +
"112322";

}
