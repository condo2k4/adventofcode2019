package hi.adventofcode;

import java.util.BitSet;

public class Day24A {
	
	public static class GameOfLife {
		private final boolean[][] tiles;

		public GameOfLife(boolean[][] tiles) {
			this.tiles = tiles;
		}
		
		public boolean isAlive(int x, int y ) {
			if(x<0 || x>=5 || y<0 || y>=5) return false;
			return tiles[y][x];
		}
		
		public int adjacentCount(int x, int y) {
			return (isAlive(x-1, y)?1:0)
					+ (isAlive(x+1, y)?1:0)
					+ (isAlive(x, y-1)?1:0)
					+ (isAlive(x, y+1)?1:0);
		}
		
		public GameOfLife next() {
			boolean[][] tiles = new boolean[5][5];
			for(int y=0; y<5; y++)
				for(int x=0; x<5; x++) {
					int count = adjacentCount(x, y);
					tiles[y][x] = this.tiles[y][x] ? count==1 : (count==1 || count==2);
				}
			return new GameOfLife(tiles);
		}
		
		public int pack() {
			int value = 0;
			int bit = 1;
			for(int y=0; y<5; y++)
				for(int x=0; x<5; x++) {
					if(tiles[y][x]) value|=bit;
					bit<<=1;
				}
			return value;
		}
	}
	
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		
		GameOfLife life = new GameOfLife(INPUT.lines()
				.map(l -> {
					boolean[] row = new boolean[5];
					for(int x=0; x<5; x++) row[x] = l.charAt(x)=='#';
					return row;
				}).toArray(boolean[][]::new));
		
		BitSet states = new BitSet(0b10000000000000000000000000);
		for(;;) {
			int packed = life.pack();
			if(states.get(packed)) {
				System.out.println(packed);
				break;
			}
			states.set(packed);
			life = life.next();
		}
		
		System.out.printf("%dms\n", System.currentTimeMillis()-start);
	}
	
	public static final String INPUT = "##.#.\n" +
"##.#.\n" +
"##.##\n" +
".####\n" +
".#...";

}
