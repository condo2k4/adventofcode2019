package hi.adventofcode;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.IntStream;

public class Day4A {
	
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		System.out.println(IntStream.rangeClosed(123257, 647015)
				.mapToObj(Integer::toString)
				.map(String::toCharArray)
				.filter(p->{
					char[] p2 = (char[])p.clone();
					Arrays.sort(p2);
					return Arrays.equals(p, p2);
				})
				.filter(p-> {
					Set<Character> cs = new HashSet<>();
					for(char c:p) cs.add(c);
					return cs.size()<p.length;
				})
				.count());
		System.out.printf("%dms\n", System.currentTimeMillis()-start);
	}

}
