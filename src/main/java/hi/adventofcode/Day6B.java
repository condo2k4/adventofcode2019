package hi.adventofcode;

import hi.adventofcode.Day6A.Body;
import hi.utils.collection.ArrayOrderedSet;
import hi.utils.collection.OrderedSet;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Day6B {
	
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		
		Map<String, Day6A.Body> bodies = Day6A.load();
		Day6A.Body santa = bodies.get("SAN");
		Day6A.Body you = bodies.get("YOU");
		
		Set<Body> common = new HashSet<>(santa.parents());
		common.retainAll(you.parents());
		
		OrderedSet<Body> santaPath = new ArrayOrderedSet<>(santa.parents());
		santaPath.removeAll(common);
		OrderedSet<Body> youPath = new ArrayOrderedSet<>(you.parents());
		youPath.removeAll(common);
		
		System.out.println(santaPath.size()+youPath.size());
		
		System.out.printf("%dms\n", System.currentTimeMillis()-start);
	}

}
