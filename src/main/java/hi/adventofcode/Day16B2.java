package hi.adventofcode;

import hi.utils.collection.primitive.HashMapOfLong;
import hi.utils.collection.primitive.MapOfLong;

import java.util.function.ToLongFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Day16B2 {
	
	private static final int[] BASE_SEQUENCE = Day16A.INPUT;
	private static final int TARGET_OFFSET = 5975483;
	private static final int TARGET_REPETITIONS = 10000;
	private static final int IGNORABLE = TARGET_OFFSET/BASE_SEQUENCE.length;
	private static final int SEQUENCE_LENGTH = BASE_SEQUENCE.length*(TARGET_REPETITIONS-IGNORABLE);
	private static final int PHASES = 100;
	private static final int DIGITS = 8;
	private static final int OFFSET = TARGET_OFFSET % BASE_SEQUENCE.length;
//	private static final int OFFSET = 5975483;
//									  6500000
//								5975483/650 = 9193 rem 33
//								10000 - 9193 = 807
	
	private static final MapOfLong<Integer>[] CACHE = new MapOfLong[PHASES+1];
	private static final ToLongFunction<Integer>[] GENERATORS = new ToLongFunction[PHASES+1];
	static {
		for(int j=1; j<=PHASES; j++) {
			final int down = j-1;
			CACHE[j] = new HashMapOfLong<>();
			GENERATORS[j] = digit->{
				int index = digit;
				int phase = digit+1;
				int result = 0;
				loop:for(;;) {
					for(int i=phase; i>0; i--) {
						result += computeDigit(index++, down);
						if(index>=SEQUENCE_LENGTH) break loop;
					}
					index+=phase;
					if(index>=SEQUENCE_LENGTH) break;
					for(int i=phase; i>0; i--) {
						result -= computeDigit(index++, down);
						if(index>=SEQUENCE_LENGTH) break loop;
					}
					index+=phase;
					if(index>=SEQUENCE_LENGTH) break;
				}
				return Math.abs(result)%10;
			};
		}
	}
	
	public static int computeDigit(int digit) { return computeDigit(digit, PHASES); }
	public static int computeDigit(int digit, int step) {
		if(step==0) return BASE_SEQUENCE[digit%BASE_SEQUENCE.length];
		if(CACHE[step].containsKey(digit)) return (int)CACHE[step].get(digit);
		synchronized(CACHE[step]) {
			return (int)CACHE[step].computeIfAbsent(digit, GENERATORS[step]);
		}
	}
	
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		
		System.out.println(IntStream.range(OFFSET, OFFSET+DIGITS)
				.parallel()
				.map(Day16B2::computeDigit)
				.mapToObj(Integer::toString)
				.collect(Collectors.joining(",")));
		
		System.out.printf("%dms\n", System.currentTimeMillis()-start);
	}

}
