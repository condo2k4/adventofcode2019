package hi.adventofcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Day21B {
	
	public static enum Outcome {
		REQUIRED_JUMP_SUCCESS, REQUIRED_JUMP_FAILED, CANT_JUMP, CAN_JUMP;
	}
	
	public static enum SimpleOutcome {
		JUMP, DONT_JUMP, DOOMED, IRRELEVANT;
	}
	
	public static Outcome checkOutcome(boolean a, boolean d) {
		if(a) return d ? Outcome.REQUIRED_JUMP_FAILED : Outcome.REQUIRED_JUMP_SUCCESS;
		return d ? Outcome.CANT_JUMP : Outcome.CAN_JUMP;
	}
	
	public static SimpleOutcome shouldJump(boolean a, boolean b, boolean c, boolean d, boolean e, boolean f, boolean g, boolean h, boolean i) {
		switch(checkOutcome(a, d)) {
			case REQUIRED_JUMP_SUCCESS: {
				SimpleOutcome doJump = shouldJump(e, f, g, h, i, false, false, false, false);
				if(doJump==SimpleOutcome.DOOMED) return SimpleOutcome.DOOMED;
				return SimpleOutcome.JUMP;
			}
			case REQUIRED_JUMP_FAILED: return SimpleOutcome.DOOMED;
			case CANT_JUMP: return SimpleOutcome.DONT_JUMP;
			case CAN_JUMP:
				if(!(b||c||d)) return SimpleOutcome.IRRELEVANT;
				
				SimpleOutcome dontJump = shouldJump(b, c, d, e, f, g, h, i, false);
				SimpleOutcome doJump = shouldJump(e, f, g, h, i, false, false, false, false);
				
				if(doJump==SimpleOutcome.DOOMED) {
					if(dontJump==SimpleOutcome.DOOMED) return SimpleOutcome.DOOMED;
					return SimpleOutcome.DONT_JUMP;
				}
				if(dontJump==SimpleOutcome.DOOMED) return SimpleOutcome.JUMP;
				
				return dontJump==SimpleOutcome.DONT_JUMP ? SimpleOutcome.JUMP : SimpleOutcome.IRRELEVANT;
		}
		throw new IllegalStateException();
		
		
//		if(d) return false; //can't do a jump
//		if(a) return true; //need to do a jump
//		if(b && (!h || (!e && !i))) return true; //good idea to jump early
//		if(c && (!h || (!e && !i))) return true; //good idea to jump early
//		return false;
	}
	
	public static boolean isInvalidScene(int x) {
		return (x&0b111000000)==0b000000000 //irrelevance
				|| (x&0b100100000)==0b100100000 //impossible
				|| (x&0b010110000)==0b010110000 //impossible
				|| (x&0b001111000)==0b001111000 //impossible
				|| (x&0b000111100)==0b000111100 //impossible
				|| (x&0b000011110)==0b000011110 //impossible
				|| (x&0b000001111)==0b000001111 //impossible
				|| (x&0b100110010)==0b100010010 //impossible second jump
				|| (x&0b110011001)==0b010001001 //impossible second jump
				//      ABCDEFGHI     ABCDEFGHI
				;
	}
	
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		System.out.println("@ABCDEFGHI");
		List<String> list = new ArrayList<>(512), parts = new ArrayList<>(9);
		for(int scene = 0; scene<512; scene++) {
			
//			if(isInvalidScene(scene)) continue;
			
			boolean a = (scene&256)==256;
			boolean b = (scene&128)==128;
			boolean c = (scene&64)==64;
			boolean d = (scene&32)==32;
			boolean e = (scene&16)==16;
			boolean f = (scene&8)==8;
			boolean g = (scene&4)==4;
			boolean h = (scene&2)==2;
			boolean i = (scene&1)==1;
			SimpleOutcome jump = shouldJump(a, b, c, d, e, f, g, h, i);
			switch(jump) {
				case DONT_JUMP:
					parts.clear();
					parts.add(a?"!a":"a");
					parts.add(b?"!b":"b");
					parts.add(c?"!c":"c");
					parts.add(d?"!d":"d");
					parts.add(e?"!e":"e");
					parts.add(f?"!f":"f");
					parts.add(g?"!g":"g");
					parts.add(h?"!h":"h");
					parts.add(i?"!i":"i");
					list.add(parts.stream().collect(Collectors.joining("*")));
					//fallthrough
				case JUMP:
					System.out.printf("#%c%c%c%c%c%c%c%c%c => %s\n", a?'.':'#', b?'.':'#', c?'.':'#', d?'.':'#', e?'.':'#', f?'.':'#', g?'.':'#', h?'.':'#', i?'.':'#', jump);
			}
		}
		System.out.println(list.stream().collect(Collectors.joining(")+\n(", "(", ")")));
		
//		IntMap<Boolean> actions = new IntMap<>(false);
//		actions.put(0,Boolean.FALSE);
//		Queue<Integer> q = new LinkedList<>();
//		q.add(1);
//		while(!q.isEmpty()) {
//			int scene = q.poll();
//			if(actions.containsKey(scene)) continue;
//			boolean jump = shouldJump((scene&256)==256, (scene&128)==128, (scene&64)==64, (scene&32)==32, (scene&16)==16, (scene&8)==8, (scene&4)==4, (scene&2)==2, (scene&1)==1);
//			actions.put(scene, jump);
//			q.add((scene<<1)&511);
//			q.add(((scene<<1)|1)&511);
//			
//		}
		
		Intcode springbot = new Intcode(Day21A.INPUT, new Intcode.AsciiInput(Arrays.stream(PROGRAM).map(String::toUpperCase).collect(Collectors.joining("\n", "", "\n"))), new Intcode.AsciiOutput());
		springbot.run();
		
		System.out.printf("%dms\n", System.currentTimeMillis()-start);
	}
	
	// *=AND  +=OR
	//JUMP		(!a*d*e)+(!a*d*h)+(!b*d*!e*h)+(!b*d*e*!f*!i)+(!c*d*!e*h)+(!c*d*e*!f*!i)=(!a+!b+!c)*(!a+!e+!f)*(!a+!e+!i)*d*(e+h)
	//DONT_JUMP	(a*b*!c*!e*!h)+(a*!d) = a*(b+!d)*(!c+!d)*(!d+!e)*(!d+!h)
	//JUMP2		(!a+!b+c+e+h)*(!a+d)
	
	public static final String[] PROGRAM = {
		"NOT B J",
		"NOT A T",
		"OR T J",
		"OR C J",
		"OR E J",
		"OR H J",
		"OR D T",
		"AND T J",
		"RUN"
	};

}
