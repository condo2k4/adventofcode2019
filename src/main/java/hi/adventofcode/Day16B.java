package hi.adventofcode;



import java.util.Arrays;
import java.util.stream.IntStream;

public class Day16B {
	
	public static int[] phase(int[] input) {
		return IntStream.rangeClosed(1, input.length).parallel()
				.map(phase -> {
					int sp=0;
					
					
//					for(int i=0; i<input.length; i++) {
//						int x = ((i+1)/phase)%4;
//						switch(x) {
//							case 1: sp+=input[i]; break;
//							case 3: sp-=input[i]; break;
//						}
//					}
					
					
//					int index = phase-1;
//loop:				for(;;) {
//						for(int i=phase; i>0; i--) {
//							sp+=input[index++];
//							if(index>=input.length) break loop;
//						}
//						index+=phase;
//						if(index>=input.length) break;
//						for(int i=phase; i>0; i--) {
//							sp-=input[index++];
//							if(index>=input.length) break loop;
//						}
//						index+=phase;
//						if(index>=input.length) break;
//					}
					
					
					int index = phase-1;

					while(index+phase*4 < input.length) {
						for(int i=phase; i>0; i--) {
							sp+=input[index++];
						}
						index+=phase;
						for(int i=phase; i>0; i--) {
							sp-=input[index++];
						}
						index+=phase;
					}

					loop: do {
						for(int i=phase; i>0; i--) {
							sp+=input[index++];
							if(index>=input.length) break loop;
						}
						index+=phase;
						if(index>=input.length) break;
						for(int i=phase; i>0 && index<input.length; i--) {
							sp-=input[index++];
						}
					} while(false);
					
					
//					for(int i=phase-1; i<input.length; i++) {
//						int x = ((i+1)/phase)%4;
//						switch(x) {
//							case 1: sp+=input[i]; break;
//							case 3: sp-=input[i]; break;
//						}
//					}
					
					
//					for(int i=phase-1; i<input.length; i++) {
//						int x = ((i+1)/phase)%4;
//						switch(x) {
//							case 1: sp+=input[i]; break;
//							case 3: sp-=input[i]; break;
//							default: i+=phase-1; break;
//						}
//					}

					return Math.abs(sp)%10;
				})
				.toArray();
	}
	
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		
		int[] input = IntStream.range(0,10000).flatMap(i->Arrays.stream(Day16A.INPUT)).toArray();
		int offset = 5975483;
		System.out.println(offset);
		System.out.println(offset+8);
		System.out.println(input.length);
		
		for(int i=0; i<100; i++) {
			long start2 = System.currentTimeMillis();
			input = phase(input);
			System.out.printf("%d = %s in %dms\n", i, Arrays.toString(Arrays.copyOfRange(input, offset, offset+8)), System.currentTimeMillis()-start2);
		}
		System.out.println(Arrays.toString(Arrays.copyOfRange(input, offset, offset+8)));
		
		System.out.printf("%dms\n", System.currentTimeMillis()-start);
	}
	
	public static final int[] SAMPLE1 = {0,3,0,3,6,7,3,2,5,7,7,2,1,2,9,4,4,0,6,3,4,9,1,5,6,5,4,7,4,6,6,4};

}

//[9, 4, 0, 1, 3, 4, 4, 1] 149ms	//[8, 3, 9, 9, 7, 0, 6, 9] 728ms	//[9, 8, 2, 1, 2, 4, 4, 5] 62s
//[9, 4, 0, 1, 3, 4, 4, 1] 127ms	//[8, 3, 9, 9, 7, 0, 6, 9] 198ms	//[9, 8, 2, 1, 2, 4, 4, 5] 2157ms	//[8, 4, 4, 6, 2, 0, 2, 6] 4m
//[9, 4, 0, 1, 3, 4, 4, 1] 134ms	//[8, 3, 9, 9, 7, 0, 6, 9] 471ms	//[9, 8, 2, 1, 2, 4, 4, 5] 31s
//[9, 4, 0, 1, 3, 4, 4, 1] 122ms	//[8, 3, 9, 9, 7, 0, 6, 9] 358ms	//[9, 8, 2, 1, 2, 4, 4, 5] 20s
																		//[9, 8, 2, 1, 2, 4, 4, 5] 2115ms	//


