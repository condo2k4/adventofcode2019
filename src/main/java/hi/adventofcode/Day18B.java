package hi.adventofcode;

import hi.utils.CollectionUtils;
import hi.utils.collection.ArrayOrderedSet;
import hi.utils.collection.OrderedSet;
import hi.utils.collection.Singleton;
import hi.utils.tuple.Tuple2;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Day18B {
	
	public static abstract class Tile {
		final int x,y;

		public Tile(int x, int y) {
			this.x = x;
			this.y = y;
		}

		public int getX() { return x; }
		public int getY() { return y; }
		public Character requiredKey() { return null; }
		public boolean isTraversable() { return true; }

		@Override
		public int hashCode() {
			int hash = 7;
			hash = 29 * hash + this.x;
			hash = 29 * hash + this.y;
			return hash;
		}

		@Override
		public boolean equals(Object obj) {
			if(this == obj) return true;
			if(obj == null || getClass() != obj.getClass()) return false;
			final Tile other = (Tile)obj;
			return this.x == other.x && this.y == other.y;
		}
	}
	
	public static class Door extends Tile {
		private final Character key;
		public Door(int x, int y, char key) { super(x,y); this.key = Character.toLowerCase(key); }
		@Override public Character requiredKey() { return key; }
	}
	
	public static class OpenSpace extends Tile {
		public OpenSpace(int x, int y) { super(x, y); }
	}
	
	public static class Key extends OpenSpace {
		private final char key;
		public Key(int x, int y, char key) { super(x,y); this.key = key; }
		@Override public int hashCode() {
			int hash = 7;
			hash = 29 * hash + this.x;
			hash = 29 * hash + this.y;
			hash = 29 * hash + this.key;
			return hash;
		}

		@Override
		public boolean equals(Object obj) {
			if(this == obj) return true;
			if(obj == null || getClass() != obj.getClass()) return false;
			final Key other = (Key)obj;
			return this.x == other.x && this.y == other.y && this.key == other.key;
		}

		@Override public String toString() { return String.valueOf(key); }
	}
	
	public static final Tile WALL = new Tile(-1,-1) {
		@Override public boolean isTraversable() { return false; }
	};
	
	public static class Route {
		final Tile start,  end;
		final Set<Character> requiredKeys;
		final int length;

		public Route(Tile start, Tile end, Set<Character> requiredKeys, int length) {
			this.start = start;
			this.end = end;
			this.requiredKeys = requiredKeys;
			this.length = length;
		}
	}
	
	public static class Quadrant {
		private final Tile start;
		private final Set<Key> keys;

		public Quadrant(Tile start, Set<Key> keys) {
			this.start = start;
			this.keys = keys;
		}
	}
	
	public static class Map {

		private final Quadrant[] quadrants;
		private final Tile[][] tiles;
		private final int maxX, maxY;
		private final HashMap<Tuple2<Tile,Tile>,Route> routes = new HashMap<>();

		private Map(Quadrant[] quadrants, Set<Key> keys, Tile[][] tiles) {
			this.quadrants = quadrants;
			this.tiles = tiles;
			maxY = tiles.length-1;
			maxX = tiles[0].length-1;
			
			for(Key k : keys) {
				for(Quadrant q : quadrants) {
					Route r = path(q.start, k);
					if(r!=null) {
						q.keys.add(k);
						routes.put(new Tuple2<>(q.start, (Tile)k), r);
						break;
					}
				}
			}
			for(Quadrant q : quadrants)
				for(Key k1 : q.keys)
					for(Key k2 : q.keys) {
						if(k1==k2) continue;
						Route r = path(k1, k2);
						routes.put(new Tuple2<>((Tile)k1, (Tile)k2), r);
						routes.put(new Tuple2<>((Tile)k2, (Tile)k1), r);
					}
		}
		
		private Set<Tile> getTraversableNeighbours(Tile t) {
			Set<Tile> set = new ArrayOrderedSet<>(4);
			if(t.x>0) {
				Tile n = tiles[t.y][t.x-1];
				if(n.isTraversable()) set.add(n);
			}
			if(t.y>0) {
				Tile n = tiles[t.y-1][t.x];
				if(n.isTraversable()) set.add(n);
			}
			if(t.x<maxX) {
				Tile n = tiles[t.y][t.x+1];
				if(n.isTraversable()) set.add(n);
			}
			if(t.y<maxY) {
				Tile n = tiles[t.y+1][t.x];
				if(n.isTraversable()) set.add(n);
			}
			return set;
		}

		public Quadrant[] getQuadrants() {
			return quadrants;
		}
		
		private Route path(Tile start, Tile dest) {
			
			class Path implements Comparable<Path>{
				final Tile current;
				final int travel;
				final int cost;
				final Set<Character> requiredKeys;
				public Path(Tile current) { this(current, null, null); }
				public Path(Tile current, Path parent, Character neededKey) {
					this.current = current;
					this.travel = parent==null?0:parent.travel+1;
					if(parent==null || parent.requiredKeys.isEmpty()) {
						requiredKeys = neededKey==null ? Collections.emptySet() : new Singleton<>(neededKey);
					} else {
						if(neededKey==null) {
							requiredKeys = parent.requiredKeys;
						} else {
							requiredKeys = new ArrayOrderedSet<>(parent.requiredKeys.size()+1);
							requiredKeys.addAll(parent.requiredKeys);
							requiredKeys.add(neededKey);
						}
					}
					cost = travel + Math.abs(current.x-dest.x)+Math.abs(current.y-dest.y);
				}
				@Override public int compareTo(Path o) { return Integer.compare(cost, o.cost); }
				public Route toRoute() {
//					System.out.printf("Pathing from (%2d,%2d) to (%2d,%2d) [%c] requires %s\n", start.x, start.y, dest.x, dest.y, ((Key)dest).key, requiredKeys);
					return new Route(start, current, requiredKeys, travel);
				}
			}
			Set<Tile> dejavu = new HashSet<>();
			PriorityQueue<Path> pq = new PriorityQueue<>();
			pq.add(new Path(start));
			while(!pq.isEmpty()) {
				Path p = pq.poll();
				if(p.current.equals(dest)) return p.toRoute();
				if(dejavu.contains(p.current)) continue;
				dejavu.add(p.current);
				for(Tile n : getTraversableNeighbours(p.current))
					pq.add(new Path(n, p, n.requiredKey()));
			}
			return null;
		}

		@Override
		public String toString() {
			return Arrays.stream(tiles)
					.map(row -> {
						Set<Tile> starts = new ArrayOrderedSet<>(4);
						for(Quadrant q : quadrants) starts.add(q.start);
						return Arrays.stream(row).map(t -> {
							if(t==WALL) return "#";
							if(t instanceof Key) return String.valueOf(((Key)t).key);
							if(t instanceof Door) return String.valueOf(Character.toUpperCase(((Door)t).key));
							
							return starts.contains(t) ? "@" : ".";
						}).collect(Collectors.joining());
					}).collect(Collectors.joining("\n"));
		}
	}
	
	public static Map loadMap(String str) {
		String[] lines = str.lines().toArray(String[]::new);
		Tile[][] tiles = new Tile[lines.length][];
		Quadrant[][] quadGrid = new Quadrant[2][2];
		Set<Key> keys = new HashSet<>();
		int height = lines.length;
		int width = lines[0].length();
		for(int y=0; y<height; y++) {
			Tile[] row = new Tile[lines[y].length()];
			for(int x=0; x<width; x++) {
				char c = lines[y].charAt(x);
				switch(c) {
					case '#': row[x] = WALL; break;
					case '.': row[x] = new OpenSpace(x, y); break;
					case '@': {
						Tile t = new OpenSpace(x, y);
						row[x] = t;
						quadGrid[x/(width/2)][y/(height/2)] = new Quadrant(t, new HashSet<>());
						break;
					}
					default:
						if(c>='a' && c<='z') {
							Key k = new Key(x,y,(char)c);
							keys.add(k);
							row[x] = k;
						} else if(c>='A' && c<='Z')
							row[x] = new Door(x,y,(char)c);
						else 
							throw new IllegalArgumentException();
				}
			}
			tiles[y]=row;
		}
		Quadrant[] quadrants = new Quadrant[]{quadGrid[0][0],quadGrid[0][1],quadGrid[1][0],quadGrid[1][1]};
		return new Map(quadrants, keys, tiles);
	}
		
	public static int path(Map map) {
		class Path implements Comparable<Path>{
			final Tile[] current;
			final int travel, estimatedCost;
			final OrderedSet<Character> collectedKeys;
			final Set<Key>[] remainingKeys;
			public Path(Quadrant[] quadrants) {
				this.current = new Tile[4];
				this.remainingKeys = new Set[4];
				for(int i=0; i<4; i++) {
					this.current[i] = quadrants[i].start;
					this.remainingKeys[i] = quadrants[i].keys;
				}
				this.travel = 0;
				this.collectedKeys = CollectionUtils.emptyOrderedSet();
				estimatedCost = travel + IntStream.range(0, 4)
						.flatMap(i->remainingKeys[i]
								.stream()
								.mapToInt(k->{
									Route r = map.routes.get(new Tuple2<>(current[i],(Tile)k));
									if(r==null) System.out.printf("%d, (%d,%d,%c) => (%d,%d,%c)\n",i, current[i].x, current[i].y, current[i] instanceof Key ? ((Key)current[i]).key : '_' , k.x, k.y, k.key);
									return r.length;
								}))
						.max()
						.orElse(0);
			}
			public Path(int quadrant, Path parent, Key current, int travel) {
				this.current = new Tile[4];
				this.remainingKeys = new Set[4];
				for(int i=0; i<4; i++) {
					if(i==quadrant) {
						this.current[i] = current;
						this.remainingKeys[i] = new ArrayOrderedSet<>(parent.remainingKeys[i]);
						this.remainingKeys[i].remove(current);
					} else {
						this.current[i] = parent.current[i];
						this.remainingKeys[i] = parent.remainingKeys[i];
					}
				}
				this.collectedKeys = new ArrayOrderedSet<>(parent.collectedKeys.size()+1);
				this.collectedKeys.addAll(parent.collectedKeys);
				this.collectedKeys.add(current.key);
				this.travel = travel + parent.travel;
				estimatedCost = this.travel + IntStream.range(0, 4).flatMap(i->remainingKeys[i].stream().mapToInt(k->map.routes.get(new Tuple2<>(this.current[i],(Tile)k)).length)).max().orElse(0);
			}
			@Override public int compareTo(Path o) { return Integer.compare(estimatedCost, o.estimatedCost); }
		}
		Set<Tuple2<Set<Character>, Character>> dejavu = new HashSet<>();
		PriorityQueue<Path> pq = new PriorityQueue<>();
		pq.add(new Path(map.quadrants));
		while(!pq.isEmpty()) {
			Path p = pq.poll();
			if(p.remainingKeys[0].isEmpty() && p.remainingKeys[1].isEmpty() && p.remainingKeys[2].isEmpty() && p.remainingKeys[3].isEmpty()) return p.travel;
//			System.out.printf("Collected: %-34s\tRemaining: %s\n", p.collectedKeys, p.remainingKeys);
			
			if(!p.collectedKeys.isEmpty()) {
				Tuple2<Set<Character>, Character> tpl = new Tuple2<>(p.collectedKeys.subList(0, p.collectedKeys.size()-1), p.collectedKeys.get(p.collectedKeys.size()-1));
				if(dejavu.contains(tpl)) continue;
				dejavu.add(tpl);
			}
			
//			System.out.printf("Using keys: %s\n", p.collectedKeys);
			for(int i=0; i<4; i++)
				for(Key k : p.remainingKeys[i]) {

					Route route = map.routes.get(new Tuple2<>(p.current[i], (Tile)k));
	//				System.out.printf("Required keys: %s\n", route==null?"[]":route.requiredKeys.toString());
					if(route!=null && p.collectedKeys.containsAll(route.requiredKeys)) {
						pq.add(new Path(i, p, k, route.length));
					}
				}
		}
		return -1;
	}
	
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		
		Map map = loadMap(INPUT);
//		System.out.println(map);
		System.out.println(path(map));
		
		System.out.printf("%dms\n", System.currentTimeMillis()-start);
	}
	
	public static final String SAMPLE1 = "########################\n" +
"#f.D.E.e.C.b.A.@.a.B.c.#\n" +
"######################.#\n" +
"#d.....................#\n" +
"########################";
	
	public static final String SAMPLE2 = "########################\n" +
"#...............b.C.D.f#\n" +
"#.######################\n" +
"#.....@.a.B.c.d.A.e.F.g#\n" +
"########################";
	
	public static final String SAMPLE3 = "#################\n" +
"#i.G..c...e..H.p#\n" +
"########.########\n" +
"#j.A..b...f..D.o#\n" +
"########@########\n" +
"#k.E..a...g..B.n#\n" +
"########.########\n" +
"#l.F..d...h..C.m#\n" +
"#################";
	
	public static final String SAMPLE4 = "########################\n" +
"#@..............ac.GI.b#\n" +
"###d#e#f################\n" +
"###A#B#C################\n" +
"###g#h#i################\n" +
"########################";
	
	public static final String SAMPLE5 = "#######\n" +
"#a.#Cd#\n" +
"##@#@##\n" +
"#######\n" +
"##@#@##\n" +
"#cB#Ab#\n" +
"#######";
	
	public static final String SAMPLE6 = "###############\n" +
"#d.ABC.#.....a#\n" +
"######@#@######\n" +
"###############\n" +
"######@#@######\n" +
"#b.....#.....c#\n" +
"###############";
	
	public static final String SAMPLE7 = "#############\n" +
"#DcBa.#.GhKl#\n" +
"#.###@#@#I###\n" +
"#e#d#####j#k#\n" +
"###C#@#@###J#\n" +
"#fEbA.#.FgHi#\n" +
"#############";
	
	public static final String SAMPLE8 = "#############\n" +
"#g#f.D#..h#l#\n" +
"#F###e#E###.#\n" +
"#dCba@#@BcIJ#\n" +
"#############\n" +
"#nK.L@#@G...#\n" +
"#M###N#H###.#\n" +
"#o#m..#i#jk.#\n" +
"#############";
	
	public static final String INPUT = "#################################################################################\n" +
"#.#.........#........s..#...#...........#.#z..#.........#...#................e..#\n" +
"#.#.#######.#.###.#####.#.#.#########.#.#.#.#.#.#.#######.#.#.###########.#####.#\n" +
"#...#.....#.#...#.#...#.#.#.....#.....#.#.#.#.#.#.........#....f#.......#.#.....#\n" +
"#.#######.#.#.###.#.###.#.#####.#.#####.#.#.#.#.#########.#######.#####.###.###C#\n" +
"#.#.......#.#.#...#...#...#...#.#...#.#.#...#.R...#.....#.#...#...#.........#...#\n" +
"#.#.#######.#.#.###.#.#####.#.#.###.#.#.#.#####.###.###.#.#.#.#.#.###########.###\n" +
"#.#.#.....#...#.#...#.....#.#.#...#.#...#.#...#.#...#...#...#.#.#.#.......#...#.#\n" +
"#.#.#.###.#.###.#.#####.#.###.###.#.#######.#.###.###.#########.###.#####.#.###.#\n" +
"#...#.#...#.#...#.#...#.#.....#...#.....#...#...#.#.#u........#j..#.#.#...#.....#\n" +
"#.###.#.#####.###.###.#.###.###.###.###.#.#####.#.#.#########.#.#P#.#.#.#######.#\n" +
"#.#...#...#...#.......#.#...#...#.....#.#...#...#x#.....#.....#.#.....#...Y.#...#\n" +
"#.#.#####.#.###.#######.#.###.#########.###.#.###.#####.#.#####.###########.#.###\n" +
"#.#...#.....#...#.....#.#.#.#.#.........#...#.#.....#...#..g..#.#.#.........#...#\n" +
"#####.#######.###.###.#.#.#.#.###.#####.#.###.#####.#.#######.#.#.#.###########.#\n" +
"#...#.#...#.#.#.#...#...#...#...#.#...#.#...#.....#.#.....#...#...#.Q.....#.....#\n" +
"#.#.#.#.#.#.#.#.###.#######.###.#.###.#.#.#.#####.#.#.###.#.#########.###.#.#####\n" +
"#.#.W.#.#.#.#...#...#...#.#.#.#.#...#...#.#.#.......#...#m..........#...#.#.#...#\n" +
"#.#####.#.#.###.#.###.#.#.#.#.#.###.#.###.#.#######################.###.#.#.###.#\n" +
"#.#.....#.#.....#.#...#.#.....#.#...#...#.#.#.......#...#.......#...#...#.#.#...#\n" +
"#.#.#####.#.#####.#.###.#.#####.#.#####.#.#.#.#####.#.#.#.#####.###.#####.#.#.###\n" +
"#k#.#...#.#...V.#...#.#.#...#...#.#...#.#.#...#...#...#...#...#...#.......#.#...#\n" +
"#.#.#.###.#####.#####.#.#####.###.#.#.#.#.#####.#.#########.#####.#########.###.#\n" +
"#.....#.N.#...#...#...#.....#.....#.#...#.#.....#.#...#.....#.....#.....#.......#\n" +
"#######.#.#.#.###.#.#######.#######.#####.###.###.#.#.#.#####.#.###.###.#.#######\n" +
"#.......#.#.#.#.#.#...#...#.........#...#.....#...#.#...#.....#.......#.#...#...#\n" +
"#.###.#####.#.#.#.###.#.#.#######.#####.#######.###.#################.#.#####.#D#\n" +
"#.#.#.#.....#.#.#...#...#.......#.B...#.#.....#...#.#...............#.#..v..#.#.#\n" +
"#.#.#.#.#####.#.###.###########.#####.#.###.#.###.#.#####.#########.#.#####.#.#.#\n" +
"#.#...#.#...#.#.....#.......#...#...#.#.#...#.#...#.#...#.#...#.#...#.....#...#.#\n" +
"#.#.###.###.#.###.#####.#.###.###.###.#.#.###.#.###.#.#.#.#.#.#.#.#############.#\n" +
"#.#...#.#...#...#.#...#.#.#...#...#...#.#.#...#.#...#.#...#.#...#.#.......#...#.#\n" +
"#.#####.#.#####.#.#.#.#.###.###.#.#.###.#.#####.#.###.#####.#####.#.#####.#.#.#.#\n" +
"#.......#.....#.#...#.#.#...#...#.#...#.#.......#.#.....#.......#...#...#...#.#.#\n" +
"#.#########.###.#####.#.#.###.###.###.#.#.#######.###.#.#.#####.#####.#.#####.#.#\n" +
"#...........#...#...#...#...#.#.....#...#.......#...#.#.#.....#...#...#...#...#.#\n" +
"###########.#.###.#.#.#####.#.#.#######.###########.###.#####.###.#.#.#####.###.#\n" +
"#...#...#...#.#...#.#.#.....#.#.#.....#.#...........#...#...#...#...#.#...#.#.I.#\n" +
"#.###.#.#.###.#.###.###.#####.###.#.###.#.###########.###.#####.#####.#.#.#.#.#.#\n" +
"#.....#.....#.....#.....#.........#....@#@............#.............#...#.....#.#\n" +
"#################################################################################\n" +
"#...#d..#.....#.....#.......#.....#.#..@#@............#.............A.#.........#\n" +
"#.#.###.#.#.###L###.#.#.###.#.###.#.#.#.#.###########.#.#.#############.###.###.#\n" +
"#.#...#...#...#.#t#.#.#...#a..#...#...#.#.#...#.....#.#.#.....#....l#.....#...#.#\n" +
"#.###.###.###.#.#.#.#.###.#####.#######.#.#.#.#####.#.#.#####.#.###.#.#######.#.#\n" +
"#.#.#o..#.#..i#...#...#.#.#.........#...#...#.......#...#...#.#.#.#...#.#.T.#.#.#\n" +
"#.#.###.#.#.#####.#####.#.#########.#.#.###########.#######.#.#.#.#####.#.#.#.#.#\n" +
"#.#...#.#.#.....#...#...#.#...#.......#.#...#.......#.......#...#.......#.#...#.#\n" +
"#.#H###.#######.###.###.#.#.#.#########.###.#.#######.###.#########.###.#.#####.#\n" +
"#.#.....#.....#.........#...#.....#b....#...#.....#.#.#.#.........#.#...#.....#.#\n" +
"#.###.###.###.###################.###.###.#######.#.#.#.#######.#.#.#########.###\n" +
"#...#.....#.#...#...#...........#...#...#...#.....#.#...#.....#.#...#.......#...#\n" +
"#.#.#######.###.#.#.#.#########.###.#######.#.#####.###.#.#.###.#.###.#####.#.#.#\n" +
"#.#.#...#...#...#.#...#.......#...#.....#...#...#.....#...#.#...#.#...#...#.#.#.#\n" +
"#.#.#.#.#.#.#.###.#####.#########.###.#.#.#####.#.#.#######.#.###.#.###.#.#.###.#\n" +
"#.#.#.#...#.......#.................#.#.#.......#.#.........#...#.#.#.#.#.#...#.#\n" +
"#.#.#.#########.#########.#########.###.#.#########.###########.#.#.#.#.#.###.#.#\n" +
"#.#.#.#.......#.#...#...#.#.......#...#.#.#.......#.#...#.......#.#.#.#.#.....#.#\n" +
"###.#.#.#####.#.#.#.#.#.#.#E#####.###.#.#.#.###.#.#.#.#.#.#########.#.#.#######.#\n" +
"#...#.#.#...#.#.#.#...#.#.#.#.....#...#.#.#.#...#.#...#.#...#.......#.#.#.....#.#\n" +
"#.###.#.#.###.#.#.#####.###.#.#####.###.#.#.#.###.#####.###.#.#######.#.###.#.#.#\n" +
"#.#...#.#...#.#.#.#...#...#.#...#.......#.#.#.#.#.#...#.#...#.#.......#.....#...#\n" +
"#.###.#.#.#.#.#.#Z#.#####.#.###.#######.#.###.#.#.#.#.#.#.#####.###.#############\n" +
"#...#.#.#.#.#.#.#...#.#r..#...#...#...#.#.#...#.#.#.#...#.........#.....#.......#\n" +
"#.#.###.###.#.#.#####.#.#####.###.#.#.#.#.#.###.#.#.###############.###.#.#####.#\n" +
"#.#.#...#...#.#.#.#...#.F.#...#...#.#.#.#.#...#.#.#.#...#.........#...#.#.#...#.#\n" +
"#.#.#.###.#.#.#.#.#.#.###.#.###.###.#.###.###.#.#.###.#.#.#######.###.#.#.#.###.#\n" +
"#.#.#...#.#.#.#...#.#...#...#.#cJ...#...#.....#.#.....#...#.#...#.#...#.#.#...#.#\n" +
"#.#.###.#.#.#.#####.###.#####.#########.#######.###########.#.#.#.#####.#.###.#.#\n" +
"#.#...#.#.#.#.......#.#...#.....#..p..#.#.....#...#.....#.....#.#.....#.#...#.#.#\n" +
"#####.#.#.###########.#.###.#.###.#.###.#.###.#.#.#.###.#.#####.#####.#.###.#K#.#\n" +
"#.....#.#...#.......U.#.....#.#...#.....#.#.#...#...#.#...#...#.#.S...#.......#.#\n" +
"#.#.###.###.#.###.###.#######.#.#########.#.#########.#####.#.#.#.#######.#####.#\n" +
"#.#.#...#...#.#.#.#.#.....#y..#...#.....#...#.........#.....#...#.......#.#...#w#\n" +
"#.###.###.###.#.#.#.#####.#.#####.#.#.#####.#.###.#####.#####.#########.#.#.#.#.#\n" +
"#...#...#.....#.#.#.......#.......#.#...#.#.#...#.#.....#.....#.......#.#n#.#.#.#\n" +
"#.#.###.#.#####.#.#####.###############.#.#.###.#.#.###########.###.###.###.#.#.#\n" +
"#.#...#.#.......#.....#.#.............M.#.#.#...#.#...#.........#...#..h#...#.O.#\n" +
"#.#.###.#############.#G###.###########.#.#.#.###.###.#.#########.###.###.#####.#\n" +
"#.#...............X...#.....#......q....#.....#.....#...........#.........#.....#\n" +
"#################################################################################";

}
