package hi.adventofcode;

import java.awt.Point;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Day17B {
	
	public static interface Cmd {
		Optional<Cmd> attemptCombine(Cmd next);
	}
	
	public static class Move implements Cmd {
		private final int distance;
		public Move(int distance) { this.distance = distance; }
		@Override public Optional<Cmd> attemptCombine(Cmd next) { return next instanceof Move ? Optional.of(new Move(distance+((Move)next).distance)) : null; }
		@Override public String toString() { return Integer.toString(distance); }

		@Override
		public boolean equals(Object obj) {
			if(this == obj) return true;
			if(obj == null || getClass() != obj.getClass()) return false;
			return this.distance == ((Move)obj).distance;
		}
	}
	
	private static final Cmd FORWARD = new Move(1);
	
	private static final Cmd LEFT = new Cmd() {
		@Override public Optional<Cmd> attemptCombine(Cmd next) { return next==RIGHT ? Optional.empty() : null; }
		@Override public String toString() { return "L"; }
	};
	
	private static final Cmd RIGHT = new Cmd() {
		@Override public Optional<Cmd> attemptCombine(Cmd next) { return next==LEFT ? Optional.empty() : null; }
		@Override public String toString() { return "R"; }
	};
	
	private static void reductiveAppend(LinkedList<Cmd> dst, Cmd cmd) {
		if(dst.isEmpty()) {
			dst.add(cmd);
		} else {
			Optional<Cmd> attemptedCombine = dst.peekLast().attemptCombine(cmd);
			if(attemptedCombine==null) {
//					//LLL=R
//				if(cmd==LEFT && dst.peekLast()==LEFT) {
//					dst.pop();
//					if(dst.peekLast()==LEFT) {
//						dst.poll();
//						dst.addLast(RIGHT);
//					} else {
//						dst.addLast(LEFT);
//						dst.addLast(LEFT);
//					}
//					//RRR=L
//				} else if(cmd==RIGHT && dst.peekLast()==RIGHT) {
//					dst.pop();
//					if(dst.peekLast()==RIGHT) {
//						dst.poll();
//						dst.addLast(LEFT);
//					} else {
//						dst.addLast(RIGHT);
//						dst.addLast(RIGHT);
//					}
//				} else
					dst.add(cmd);
			} else {
				dst.removeLast();
				if(attemptedCombine.isPresent())
					dst.addLast(attemptedCombine.get());
			}
		}
	}
	
	public static List<Cmd> reduce(List<Cmd> cmds) {
		LinkedList<Cmd> result = new LinkedList<>();
		for(Cmd cmd : cmds) reductiveAppend(result, cmd);
		return result;
	}
	
	public static List<Cmd> explode(List<Cmd> cmds) {
		LinkedList<Cmd> result = new LinkedList<>();
		for(Cmd cmd : cmds) {
			if(cmd instanceof Move) {
				for(int i=((Move)cmd).distance; i>0; i--)
					result.add(FORWARD);
			} else {
				result.add(cmd);
			}
		}
		return result;
	}
	
	public static String toString(List<Cmd> cmds) {
		return cmds.stream().map(Cmd::toString).collect(Collectors.joining(","));
	}
	
	public static List<Cmd> fromString(String sequence) {
		return Arrays.stream(sequence.split(",")).map(t->{
			switch(t) {
				case "L": return LEFT;
				case "R": return RIGHT;
				default: return new Move(Integer.parseInt(t));
			}
		}).collect(Collectors.toList());
	}
	
	public static List<Cmd> expandMainSequence(List<Cmd> A, List<Cmd> B, List<Cmd> C, String sequence) {
		List<Cmd> concatinated = reduce(Arrays.stream(sequence.split(",")).flatMap(t-> {
			switch(t) {
				case "A": return A.stream();
				case "B": return B.stream();
				case "C": return C.stream();
				case "": return Stream.empty();
				default: throw new IllegalArgumentException(t);
			}
		}).collect(Collectors.toList()));
//		int i = concatinated.size()-1;
//		while(concatinated.get(i)==LEFT || concatinated.get(i)==RIGHT)
//			i--;
//		i++;
//		return i==concatinated.size() ? concatinated : concatinated.subList(0, i);
		return concatinated;
	}
	
	private static <T> boolean startsWith(List<T> sequence, List<T> subsequence) {
		if(sequence.size()<subsequence.size()) return false;
		Iterator<T> a = sequence.iterator();
		Iterator<T> b = subsequence.iterator();
		while(b.hasNext()) {
			if(!a.next().equals(b.next())) return false;
		}
		return true;
	}
	
	private static <T> boolean endsWith(List<T> sequence, T terminal) {
		if(sequence.isEmpty()) return false;
		return sequence.get(sequence.size()-1).equals(terminal);
	}
	
	private static List<LinkedList<String>> extractMainSequenceInternal(List<Cmd> A, List<Cmd> B, List<Cmd> C, List<Cmd> target) {
		List<LinkedList<String>> results = new LinkedList<>();
		if(startsWith(target, A)) {
			for(LinkedList<String> s : extractMainSequenceInternal(A,B,C,target.subList(A.size(), target.size()))) {
				s.addFirst("A");
				results.add(s);
			}
		}
		if(endsWith(A, LEFT)) {
			List<Cmd> subSeq = A.subList(0, A.size()-1);
			if(startsWith(target, subSeq)) {
				LinkedList<Cmd> subList = new LinkedList<>(target.subList(subSeq.size(), target.size()));
				subList.addFirst(RIGHT);
				for(LinkedList<String> s : extractMainSequenceInternal(A,B,C,subList)) {
					s.addFirst("A");
					results.add(s);
				}
			}
		}
		if(endsWith(A, RIGHT)) {
			List<Cmd> subSeq = A.subList(0, A.size()-1);
			if(startsWith(target, subSeq)) {
				LinkedList<Cmd> subList = new LinkedList<>(target.subList(subSeq.size(), target.size()));
				subList.addFirst(LEFT);
				for(LinkedList<String> s : extractMainSequenceInternal(A,B,C,subList)) {
					s.addFirst("A");
					results.add(s);
				}
			}
		}
		if(startsWith(target, B)) {
			for(LinkedList<String> s : extractMainSequenceInternal(A,B,C,target.subList(B.size(), target.size()))) {
				s.addFirst("B");
				results.add(s);
			}
		}
		if(endsWith(B, LEFT)) {
			List<Cmd> subSeq = B.subList(0, B.size()-1);
			if(startsWith(target, subSeq)) {
				LinkedList<Cmd> subList = new LinkedList<>(target.subList(subSeq.size(), target.size()));
				subList.addFirst(RIGHT);
				for(LinkedList<String> s : extractMainSequenceInternal(A,B,C,subList)) {
					s.addFirst("A");
					results.add(s);
				}
			}
		}
		if(endsWith(B, RIGHT)) {
			List<Cmd> subSeq = B.subList(0, B.size()-1);
			if(startsWith(target, subSeq)) {
				LinkedList<Cmd> subList = new LinkedList<>(target.subList(subSeq.size(), target.size()));
				subList.addFirst(LEFT);
				for(LinkedList<String> s : extractMainSequenceInternal(A,B,C,subList)) {
					s.addFirst("A");
					results.add(s);
				}
			}
		}
		if(startsWith(target, C)) {
			for(LinkedList<String> s : extractMainSequenceInternal(A,B,C,target.subList(C.size(), target.size()))) {
				s.addFirst("C");
				results.add(s);
			}
		}
		if(endsWith(C, LEFT)) {
			List<Cmd> subSeq = C.subList(0, C.size()-1);
			if(startsWith(target, subSeq)) {
				LinkedList<Cmd> subList = new LinkedList<>(target.subList(subSeq.size(), target.size()));
				subList.addFirst(RIGHT);
				for(LinkedList<String> s : extractMainSequenceInternal(A,B,C,subList)) {
					s.addFirst("A");
					results.add(s);
				}
			}
		}
		if(endsWith(C, RIGHT)) {
			List<Cmd> subSeq = C.subList(0, C.size()-1);
			if(startsWith(target, subSeq)) {
				LinkedList<Cmd> subList = new LinkedList<>(target.subList(subSeq.size(), target.size()));
				subList.addFirst(LEFT);
				for(LinkedList<String> s : extractMainSequenceInternal(A,B,C,subList)) {
					s.addFirst("A");
					results.add(s);
				}
			}
		}
		if(results.isEmpty()) results.add(new LinkedList<>());
		return results;
	}
	
	private static int mainSequenceLength(List<Cmd> A, List<Cmd> B, List<Cmd> C, List<String> main) {
		return main.stream().mapToInt(t->{
			switch(t) {
				case "A": return A.size();
				case "B": return B.size();
				case "C": return C.size();
				default: return 0;
			}
		}).sum();
	}
	
	public static String extractMainSequence(List<Cmd> A, List<Cmd> B, List<Cmd> C, List<Cmd> target) {
		return extractMainSequenceInternal(A, B, C, target).stream()
				.max((LinkedList<String> p, LinkedList<String> q) -> {
					int resLen = Integer.compare(mainSequenceLength(A,B,C,p), mainSequenceLength(A,B,C,q));
					return resLen==0 ? Integer.compare(p.size(), q.size()) : resLen;
				})
				.orElse(new LinkedList<>())
				.stream()
				.collect(Collectors.joining(","));
	}
	
	public static enum Heading {
		UP,DOWN,LEFT,RIGHT
	}
	
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		
		//Generate target path
		char[][] map = Day17A.generateMap();
		Point p = null;
		for(int i=0; i<map.length; i++)
			for(int j=0; j<map[i].length; j++)
				if(map[i][j]=='^')
					p = new Point(j,i);
		LinkedList<Cmd> targetCommands = new LinkedList<>();
		Heading heading = Heading.UP;
		for(;;) {
			switch(heading) {
				case UP: {
					if(p.y>0 && map[p.y-1][p.x]=='#') {
						reductiveAppend(targetCommands, new Move(1));
						p = new Point(p.x, p.y-1);
						continue;
					}
					if(p.x>0 && map[p.y][p.x-1]=='#') {
						reductiveAppend(targetCommands, LEFT);
						targetCommands.add(new Move(1));
						p = new Point(p.x-1, p.y);
						heading = Heading.LEFT;
						continue;
					}
					if(p.x<54 && map[p.y][p.x+1]=='#') {
						reductiveAppend(targetCommands, RIGHT);
						targetCommands.add(new Move(1));
						p = new Point(p.x+1, p.y);
						heading = Heading.RIGHT;
						continue;
					}
					break;
				}
				case DOWN: {
					if(p.y<36 && map[p.y+1][p.x]=='#') {
						reductiveAppend(targetCommands, new Move(1));
						p = new Point(p.x, p.y+1);
						continue;
					}
					if(p.x>0 && map[p.y][p.x-1]=='#') {
						reductiveAppend(targetCommands, RIGHT);
						targetCommands.add(new Move(1));
						p = new Point(p.x-1, p.y);
						heading = Heading.LEFT;
						continue;
					}
					if(p.x<54 && map[p.y][p.x+1]=='#') {
						reductiveAppend(targetCommands, LEFT);
						targetCommands.add(new Move(1));
						p = new Point(p.x+1, p.y);
						heading = Heading.RIGHT;
						continue;
					}
					break;
				}
				case LEFT: {
					if(p.x>0 && map[p.y][p.x-1]=='#') {
						reductiveAppend(targetCommands, new Move(1));
						p = new Point(p.x-1, p.y);
						continue;
					}
					if(p.y>0 && map[p.y-1][p.x]=='#') {
						reductiveAppend(targetCommands, RIGHT);
						targetCommands.add(new Move(1));
						p = new Point(p.x, p.y-1);
						heading = Heading.UP;
						continue;
					}
					if(p.y<36 && map[p.y+1][p.x]=='#') {
						reductiveAppend(targetCommands, LEFT);
						targetCommands.add(new Move(1));
						p = new Point(p.x, p.y+1);
						heading = Heading.DOWN;
						continue;
					}
					break;
				}
				case RIGHT: {
					if(p.x<54 && map[p.y][p.x+1]=='#') {
						reductiveAppend(targetCommands, new Move(1));
						p = new Point(p.x+1, p.y);
						continue;
					}
					if(p.y>0 && map[p.y-1][p.x]=='#') {
						targetCommands.add(LEFT);
						targetCommands.add(new Move(1));
						p = new Point(p.x, p.y-1);
						heading = Heading.UP;
						continue;
					}
					if(p.y<36 && map[p.y+1][p.x]=='#') {
						targetCommands.add(RIGHT);
						reductiveAppend(targetCommands, new Move(1));
						p = new Point(p.x, p.y+1);
						heading = Heading.DOWN;
						continue;
					}
					break;
				}
			}
			//end of path
			break;
		}
//		System.out.println(toString(targetCommands));
		{
			List<Cmd> LLL = Arrays.asList(LEFT,LEFT,LEFT);
			System.out.println(targetCommands.stream()
					.flatMap(cmd->cmd==RIGHT?LLL.stream():Stream.of(cmd))
					.map(Cmd::toString)
					.collect(Collectors.joining(",")));
		}
		List<Cmd> exploded = new ArrayList<>(explode(targetCommands));
		exploded.add(LEFT);
		
//		int aSize = 7;
//		int bSize = 1;
//		int cSize = 22;
//		List<Cmd> A = exploded.subList(0, aSize);
//		List<Cmd> B = exploded.subList(aSize, aSize+bSize);
//		List<Cmd> C = exploded.subList(exploded.size()-cSize, exploded.size());

//		List<Cmd> A = explode(fromString("L,6,R"));
//		List<Cmd> B = explode(fromString("L,L,8,L,10,L"));
//		List<Cmd> C = explode(fromString("8,R,12"));
//		String main = extractMainSequence(A, B, C, exploded);
//		[L,6,R,8,R,12,L,6,L,8,L,10,L,8,R,12,L,6,R,8,R,12,L,6,R]

//		List<Cmd> A = explode(fromString("L,6"));
//		List<Cmd> B = explode(fromString("L,8,L,10,L,L"));
//		List<Cmd> C = explode(fromString("R,8,R,12"));
//		String main = "A,C,A,B,C,A,C,A,A";
//		[L,6,R,8,R,12,L,6,L,8,L,10,L,8,R,12,L,6,R,8,R,12,L,6,L,6]

//		List<Cmd> A = explode(fromString("L,6"));
//		List<Cmd> B = explode(fromString("L,8,L,10,L,L"));
//		List<Cmd> C = explode(fromString("R,8,R,12,L,6"));
//		String main = "A,C,B,C,C,A";
//		[L,6,R,8,R,12,L,6,L,8,L,10,L,8,R,12,L,6,R,8,R,12,L,6,L,6]

//		List<Cmd> A = explode(fromString("L,6"));
//		List<Cmd> B = explode(fromString("L,8,L,10,L,L"));
//		List<Cmd> C = explode(fromString("R,8,R,12"));
//		String main = "A,C,A,B,C,A,C,A,B";
//		[L,6,R,8,R,12,L,6,L,8,L,10,L,8,R,12,L,6,R,8,R,12,L,6,L,8,L,10,L,L]

		List<Cmd> A = explode(fromString("L,6,L,L"));
		List<Cmd> B = explode(fromString("L,L,L,8,L8"));
		List<Cmd> C = explode(fromString("L,8,L,L,L,12"));
		String main = "A,C,A,B,C,A,C,A,B";
//		[A,C,A,B,2,C,A,C,A,B,L,10,A,L,L,A,L,L,L,10,C,L,8,L,10,A,L,L,A,L,L,L,10,C,A,C,A,B,L,10,A,L,L,A,L,L,L,10,C] 

//		L,6,L,L,L,8,L,L,L,12,L,6,L,8,L,10,L,8,L,L,L,12,L,6,L,L,L,8,L,L,L,12,L,6,L,8,L,8,L,10,L,6,L,6,L,10,L,8,L,L,L,12,L,8,L,10,L,6,L,6,L,10,L,8,L,L,L,12,L,6,L,L,L,8,L,L,L,12,L,6,L,8,L,8,L,10,L,6,L,6,L,10,L,8,L,L,L,12
		
		
		List<Cmd> produced = reduce(expandMainSequence(A,B,C,main));
		
		String aStr = toString(reduce(A));
		String bStr = toString(reduce(B));
		String cStr = toString(reduce(C));
		
		System.out.println();
		System.out.printf("A=[%s] (%d)\n", aStr, aStr.length());
		System.out.printf("B=[%s] (%d)\n", bStr, bStr.length());
		System.out.printf("C=[%s] (%d)\n", cStr, cStr.length());
		System.out.printf("Main=[%s] (%d)\n", main, main.length());
		System.out.printf("Result=[%s] (%b)\n", toString(produced), produced.equals(targetCommands));
		System.out.println();
		
		StringBuilder b = new StringBuilder();
		b.append(main).append('\n');
		b.append(aStr).append('\n');
		b.append(bStr).append('\n');
		b.append(cStr).append('\n');
		b.append(CAMERA).append('\n');
		
		long[] prog = Day17A.INPUT.clone();
		prog[0]=2;
		Reader reader = new StringReader(b.toString());
		new Intcode(prog, () -> {
			try {
				return reader.read();
			} catch(IOException ex) { throw new IllegalStateException(ex); }
		}, l->{
			if(l<256) System.out.write((int)l);
			else System.out.println(l);
		})
				.run()
				;
		
		System.out.printf("%dms\n", System.currentTimeMillis()-start);
	}
	
	private static final String CAMERA = "n";
}


//L,6,R,8,R,12,L,6,L,8,L,10,L,8,R,12,L,6,R,8,R,12,L,6,L,8,L,8,L,10,L,6,L,6,L,10,L,8,R,12,L,8,L,10,L,6,L,6,L,10,L,8,R,12,L,6,R,8,R,12,L,6,L,8,L,8,L,10,L,6,L,6,L,10,L,8,R,12
