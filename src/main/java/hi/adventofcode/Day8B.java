package hi.adventofcode;

import hi.utils.assets.vis.FrameStore;
import hi.utils.gui.ComponentFactory;
import hi.utils.gui.components.HIImage;

import java.awt.Color;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.swing.JFrame;

public class Day8B {
	
	public static BufferedImage toImage(List<char[][]> raw) {
		int height = raw.get(0).length;
		int width = raw.get(0)[0].length;
		
		BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		
		for(int y=0; y<height; y++) {
			for(int x=0; x<width; x++) {
				int p, l=0;
				do {
					p = raw.get(l++)[y][x]-'0';
				} while(p==2);
				img.setRGB(x, y, (p==1?Color.WHITE:Color.BLACK).getRGB());
			}
		}
		
		return img;
	}
	
	public static void main(String[] args) throws IOException {
		long start = System.currentTimeMillis();
		
		List<char[][]> img = Day8A.loadImage(new File("Day8.txt"),25,6);
		
		JFrame frame = new JFrame("Password");
		HIImage before = new HIImage(new FrameStore("Image", toImage(img)), true, false, true);
		before.setInterpolate(RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR);
		frame.add(ComponentFactory.scrollPane(before));
		frame.setSize(70*25,70*6+50);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
		System.out.printf("%dms\n", System.currentTimeMillis()-start);
	}

}
