package hi.adventofcode;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.ToDoubleFunction;

public class AStar {
	
	public static <N, E> List<E> route(N start, N end, Function<N, ? extends Collection<E>> getEdges, BiFunction<E, N, N> getOpposite, ToDoubleFunction<E> getEdgeCost, ToDoubleBiFunction<N,N> getEstimateRemaininCost) {
		
		if(start==end) return Collections.emptyList();
		
		class PartialPath implements Comparable<PartialPath> {
			final PartialPath parent;
			final E edge;
			final N farNode;
			final double costSoFar;
			final double estimatedRemaining;
			
			public PartialPath(E edge, N farNode) {
				this.parent = null;
				this.edge = edge;
				this.farNode = farNode;
				this.costSoFar = getEdgeCost.applyAsDouble(edge);
				this.estimatedRemaining = getEstimateRemaininCost.applyAsDouble(farNode, end);
			}
			
			public PartialPath(PartialPath parent, E edge, N farNode) {
				this.parent = parent;
				this.edge = edge;
				this.farNode = farNode;
				this.costSoFar = parent.costSoFar + getEdgeCost.applyAsDouble(edge);
				this.estimatedRemaining = getEstimateRemaininCost.applyAsDouble(farNode, end);
			}

			@Override
			public int compareTo(PartialPath o) {
				return Double.compare(costSoFar+estimatedRemaining, o.costSoFar+o.estimatedRemaining);
			}
			
			public List<E> toList(List<E> l) {
				l.add(0, edge);
				if(parent!=null) parent.toList(l);
				return l;
			}
		}
		
		
		PriorityQueue<PartialPath> pq = new PriorityQueue();
		for(E edge : getEdges.apply(start)) {
			pq.add(new PartialPath(edge, getOpposite.apply(edge, start)));
		}
		
		Set<N> visited = new HashSet<>();
		visited.add(start);
		
		while(!pq.isEmpty()) {
			PartialPath pp = pq.poll();
			if(pp.farNode.equals(end)) return pp.toList(new LinkedList<>());
			
			if(visited.contains(pp.farNode)) continue;
			visited.add(pp.farNode);
			
			for(E edge : getEdges.apply(pp.farNode)) {
				N farNode = getOpposite.apply(edge, pp.farNode);
				if(visited.contains(farNode)) continue;
				pq.add(new PartialPath(pp, edge, farNode));
			}
		}
		
		return null;
	}
	
	public static <N> List<N> route(N start, N end, Function<N, ? extends Collection<N>> getNeighbours, ToDoubleBiFunction<N,N> getEdgeCost, ToDoubleBiFunction<N,N> getEstimateRemaininCost) {
		
		if(start==end) return Collections.emptyList();
		
		class PartialPath implements Comparable<PartialPath> {
			final PartialPath parent;
			final N farNode;
			final double costSoFar;
			final double estimatedRemaining;
			
			public PartialPath(N farNode) {
				this.parent = null;
				this.farNode = farNode;
				this.costSoFar = getEdgeCost.applyAsDouble(start,farNode);
				this.estimatedRemaining = getEstimateRemaininCost.applyAsDouble(farNode, end);
			}
			
			public PartialPath(PartialPath parent, N farNode) {
				this.parent = parent;
				this.farNode = farNode;
				this.costSoFar = parent.costSoFar + getEdgeCost.applyAsDouble(parent.farNode, farNode);
				this.estimatedRemaining = getEstimateRemaininCost.applyAsDouble(farNode, end);
			}

			@Override
			public int compareTo(PartialPath o) {
				return Double.compare(costSoFar+estimatedRemaining, o.costSoFar+o.estimatedRemaining);
			}
			
			public List<N> toList(List<N> l) {
				l.add(0, farNode);
				if(parent!=null) parent.toList(l);
				else l.add(0, start);
				return l;
			}
		}
		
		PriorityQueue<PartialPath> pq = new PriorityQueue();
		for(N n : getNeighbours.apply(start)) {
			pq.add(new PartialPath(n));
		}
		
		Set<N> visited = new HashSet<>();
		visited.add(start);
		
		while(!pq.isEmpty()) {
			PartialPath pp = pq.poll();
			if(pp.farNode.equals(end)) return pp.toList(new LinkedList<>());
			
			if(visited.contains(pp.farNode)) continue;
			visited.add(pp.farNode);
			
			for(N farNode : getNeighbours.apply(pp.farNode)) {
				if(visited.contains(farNode)) continue;
				pq.add(new PartialPath(pp, farNode));
			}
		}
		
		return null;
	}
	
	@FunctionalInterface
	public static interface ToDoubleBiFunction<T, U> {

		/**
		 * Applies this function to the given arguments.
		 *
		 * @param t the first function argument
		 * @param u the second function argument
		 * @return the function result
		 */
		double applyAsDouble(T t, U u);
	}

}