package hi.adventofcode;

import hi.adventofcode.Intcode.Pipe;

import java.io.IOException;
import java.util.Arrays;

public class Day7B {
	
	private static void insert(int value, int[] arr, int index) {
		int actual = 0;
		while(arr[actual]>=0) actual++;
		while(index>0) {
			index--;
			actual++;
			while(arr[actual]>=0) actual++;
		}
		arr[actual]=value;
	}
	
	public static int[] permutation(int zero, int one, int two, int three) {
		int[] res = new int[]{-1,-1,-1,-1,-1};
		
		insert(5, res, zero);
		insert(6, res, one);
		insert(7, res, two);
		insert(8, res, three);
		insert(9, res, 0);
		
		return res;
	}
	
	public static long testSequence(long[] program, int[] sequence) {
		Pipe pAB = new Pipe();
		Pipe pBC = new Pipe();
		Pipe pCD = new Pipe();
		Pipe pDE = new Pipe();
		Pipe pEA = new Pipe();
		
		Intcode ampA = new Intcode((long[])program.clone(), new Intcode.PrefixSupplier(new long[]{sequence[0],0}, pEA), pAB);
		Intcode ampB = new Intcode((long[])program.clone(), new Intcode.PrefixSupplier(new long[]{ sequence[1] }, pAB), pBC);
		Intcode ampC = new Intcode((long[])program.clone(), new Intcode.PrefixSupplier(new long[]{ sequence[2] }, pBC), pCD);
		Intcode ampD = new Intcode((long[])program.clone(), new Intcode.PrefixSupplier(new long[]{ sequence[3] }, pCD), pDE);
		Intcode ampE = new Intcode((long[])program.clone(), new Intcode.PrefixSupplier(new long[]{ sequence[4] }, pDE), pEA);
		
		Intcode.runAndWait(ampA, ampB, ampC, ampD, ampE);
		return pEA.getAsLong();
	}
	
	public static void main(String[] args) throws IOException {
		long start = System.currentTimeMillis();
		
		long max = 0;
		for(int a=0; a<5; a++) for(int b=0; b<4; b++) for(int c=0; c<3; c++) for(int d=0; d<2; d++) {
			int[] perm = permutation(a,b,c,d);
			long res = testSequence(INPUT, perm);
			System.out.printf("%s: %d\n", Arrays.toString(perm), res);
			if(res>max) max=res;
		}
		System.out.printf("Max: %d\n", max);
		
		System.out.printf("%dms\n", System.currentTimeMillis()-start);
	}
	
	public static final long[] INPUT_SAMPLE1 = {3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5};
	public static final long[] INPUT_SAMPLE2 = {3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10};
	
	public static final long[] INPUT = {3,8,1001,8,10,8,105,1,0,0,21,30,39,64,81,102,183,264,345,426,99999,3,9,1001,9,2,9,4,9,99,3,9,1002,9,4,9,4,9,99,3,9,1002,9,5,9,101,2,9,9,102,3,9,9,1001,9,2,9,1002,9,2,9,4,9,99,3,9,1002,9,3,9,1001,9,5,9,1002,9,3,9,4,9,99,3,9,102,4,9,9,1001,9,3,9,102,4,9,9,1001,9,5,9,4,9,99,3,9,101,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,1,9,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,1,9,4,9,3,9,102,2,9,9,4,9,99,3,9,101,1,9,9,4,9,3,9,101,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,1,9,9,4,9,99,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,99,3,9,101,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,101,1,9,9,4,9,99};

}
