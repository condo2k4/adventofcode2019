package hi.adventofcode;

import java.io.IOException;
import java.util.function.LongConsumer;

public class Day7A {
	
	public static int[] permutation(int zero, int one, int two, int three) {
		int[] res = new int[5];
		int four = 0;
		res[zero] = 0;
		if(one>=zero) one++;
		if(two>=zero) two++;
		if(three>=zero) three++;
		if(four>=zero) four++;
		res[one] = 1;
		if(two>=one) two++;
		if(three>=one) three++;
		if(four>=one) four++;
		res[two] = 2;
		if(three>=two) three++;
		if(four>=two) four++;
		res[three] = 3;
		if(four>=three) four++;
		res[four] = 4;
		return res;
	}
	
	public static long testSequence(int ... sequence) {
		long[] buffer = {0};
		LongConsumer store = x -> buffer[0]=x;
		for(int s : sequence) {
			Intcode amp = new Intcode((long[])INPUT.clone());
			amp.setIn(s,buffer[0]);
			amp.setOut(store);
			amp.run();
		}
		return buffer[0];
	}
	
	public static void main(String[] args) throws IOException {
		long start = System.currentTimeMillis();
		
		long max = 0;
		for(int a=0; a<5; a++) for(int b=0; b<4; b++) for(int c=0; c<3; c++) for(int d=0; d<2; d++) {
			long res = testSequence(permutation(a,b,c,d));
			if(res>max) max=res;
		}
		System.out.println(max);
		
		System.out.printf("%dms\n", System.currentTimeMillis()-start);
	}
	
	public static final long[] INPUT_SAMPLE = {3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0};
	
	public static final long[] INPUT = {3,8,1001,8,10,8,105,1,0,0,21,30,39,64,81,102,183,264,345,426,99999,3,9,1001,9,2,9,4,9,99,3,9,1002,9,4,9,4,9,99,3,9,1002,9,5,9,101,2,9,9,102,3,9,9,1001,9,2,9,1002,9,2,9,4,9,99,3,9,1002,9,3,9,1001,9,5,9,1002,9,3,9,4,9,99,3,9,102,4,9,9,1001,9,3,9,102,4,9,9,1001,9,5,9,4,9,99,3,9,101,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,1,9,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,1,9,4,9,3,9,102,2,9,9,4,9,99,3,9,101,1,9,9,4,9,3,9,101,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,1,9,9,4,9,99,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,99,3,9,101,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,101,1,9,9,4,9,99};

}
