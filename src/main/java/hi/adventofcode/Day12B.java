package hi.adventofcode;

import hi.utils.math.SoftMath;

import java.util.Arrays;


public class Day12B {
	
	public static long lcm(long a, long b) {
		if(a == 0 || b == 0) return 0;
		long absNumber1 = Math.abs(a);
		long absNumber2 = Math.abs(b);
		long absHigherNumber = Math.max(absNumber1, absNumber2);
		long absLowerNumber = Math.min(absNumber1, absNumber2);
		long lcm = absHigherNumber;
		while(lcm % absLowerNumber != 0) lcm += absHigherNumber;
		return lcm;
	}
	
	public static long lcm(long ... vals) {
		switch(vals.length) {
			case 0: throw new IllegalArgumentException();
			case 1: return vals[0];
			case 2: return lcm(vals[0], vals[1]);
			default: {
				long value = lcm(vals[0], vals[1]);
				int index = 2;
				while(index<vals.length)
					value = lcm(value, vals[index++]);
				return value;
			}
		}
	}
	
	public static class State {
		private Moon[] moons;

		public State(Moon[] moons) {
			this.moons = moons;
		}
		
		public int energy() {
			int sum = 0;
			for(Moon moon : moons) sum+=moon.energy();
			return sum;
		}
		
		@Override
		public State clone() {
			Moon[] newMoons = new Moon[moons.length];
			for(int i=0; i<newMoons.length; i++) newMoons[i] = new Moon(moons[i]);
			return new State(newMoons);
		}
		
		public State nextState() {
			State nextState = clone();
			nextState.tick();
			return nextState;
		}
		
		public void tick() {
			for(int i=0; i<moons.length; i++) for(int j=i+1; j<moons.length; j++) moons[i].gravitate(moons[j]);
			for(Moon moon : moons) moon.step();
		}

//		@Override
//		public int hashCode() {
//			int hash = 3;
//			hash = 41 * hash + Arrays.deepHashCode(this.moons);
//			return hash;
//		}
		
		public boolean equal(State other) {
			return Arrays.deepEquals(this.moons, other.moons);
		}

//		@Override
//		public boolean equals(Object obj) {
//			if(this == obj) return true;
//			if(obj == null || getClass() != obj.getClass()) return false;
//			return equal((State)obj);
//		}
	}
	
	public static class Moon {
		private int posX, posY, posZ;
		private int velX, velY, velZ;

		public Moon(Moon other) { this(other.posX, other.posY, other.posZ, other.velX, other.velY, other.velZ); }
		public Moon(int posX, int posY, int posZ) { this(posX, posY, posZ, 0, 0, 0); }
		private Moon(int posX, int posY, int posZ, int velX, int velY, int velZ) {
			this.posX = posX;
			this.posY = posY;
			this.posZ = posZ;
			this.velX = velX;
			this.velY = velY;
			this.velZ = velZ;
		}
		
		public void gravitate(Moon other) {
//			int x = SoftMath.signum(other.posX-posX);
//			velX+=x;
//			other.velX-=x;
			
//			int y = SoftMath.signum(other.posY-posY);
//			velY+=y;
//			other.velY-=y;
			
			int z = SoftMath.signum(other.posZ-posZ);
			velZ+=z;
			other.velZ-=z;
		}
		
		public void step() {
			posX+=velX;
			posY+=velY;
			posZ+=velZ;
		}
		
		public int energy() {
			int kineticEnergy = Math.abs(posX)+Math.abs(posY)+Math.abs(posZ);
			int potentialEnergy = Math.abs(velX)+Math.abs(velY)+Math.abs(velZ);
			return kineticEnergy*potentialEnergy;
		}

		@Override
		public String toString() {
			return String.format("pos=<x=%2d,y=%2d,z=%2d>, vel=<x=%2d,y=%2d,z=%2d>", posX, posY, posZ, velX, velY, velZ);
		}

		@Override
		public int hashCode() {
			int hash = 3;
			hash = 19 * hash + this.posX;
			hash = 19 * hash + this.posY;
			hash = 19 * hash + this.posZ;
			hash = 19 * hash + this.velX;
			hash = 19 * hash + this.velY;
			hash = 19 * hash + this.velZ;
			return hash;
		}

		@Override
		public boolean equals(Object obj) {
			if(this == obj) return true;
			if(obj == null || getClass() != obj.getClass()) return false;
			final Moon other = (Moon)obj;
			return this.posX == other.posX
					&& this.posY == other.posY
					&& this.posZ == other.posZ
					&& this.velX == other.velX
					&& this.velY == other.velY
					&& this.velZ == other.velZ;
		}
		
	}
	
	public static int oribtalPeriod(Moon moon, Moon[] moons) {
		int index = -1;
		for(int i=0; i<moons.length; i++)
			if(moons[i].equals(moon)) {
				index = i;
				break;
			}
		if(index==-1) throw new IllegalArgumentException();
		return oribtalPeriod(index, moons);
	}
	
	public static int oribtalPeriod(int index, Moon[] moons) {
		if(index==-1) throw new IllegalArgumentException();
		Moon initial = new Moon(moons[index]);
		
		State s = new State(moons).nextState();
		int time = 1;
		while(!initial.equals(s.moons[index])) {
			s.tick();
			time++;
		}
		return time;
	}
	
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		
		Moon[] moons = SAMPLE2;
		for(Moon moon : moons) {
			System.out.println(oribtalPeriod(moon, moons));
		}
		
		System.out.printf("%dms\n", System.currentTimeMillis()-start);
	}
	
	public static final Moon[] INPUT = {
		new Moon( 5,13,-3),
		new Moon(18,-7,13),
		new Moon(16, 3, 4),
		new Moon( 0, 8, 8)
	};
	
	public static final Moon[] SAMPLE1 = {
		new Moon(-1,  0, 2),
		new Moon( 2,-10,-7),
		new Moon( 4, -8, 8),
		new Moon( 3,  5,-1)
	};
	
	public static final Moon[] SAMPLE2 = {
		new Moon(-8,-10, 0),
		new Moon( 5,  5,10),
		new Moon( 2, -7, 3),
		new Moon( 9, -8,-3)
	};

}
