package hi.adventofcode;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Day8A {
	
	public static List<char[][]> loadImage(File f, int width, int height) throws IOException {
		List<char[][]> img = new ArrayList<>();
		try(Reader in = new FileReader(f)) {
			for(;;) {
				char[][] layer = loadLayer(in, width, height);
				if(layer==null) return img;
				img.add(layer);
			}
		}
	}
	
	private static char[][] loadLayer(Reader in, int width, int height) throws IOException {
		char[][] layer = new char[height][width];
		for(int line=0; line<height; line++) {
			if(in.read(layer[line])!=width) return null;
		}
		return layer;
	}
	
	public static void main(String[] args) throws IOException {
		long start = System.currentTimeMillis();
		
		List<char[][]> img = loadImage(new File("Day8.txt"),25,6);
		
		int[] counts = img.stream()
				.map( (char[][] layer) -> {
					int[] count = new int[3];
					for(char[] l : layer)
						for(char c : l)
							count[c-'0']++;
					return count;
				})
				.min(Comparator.comparingInt(cs->cs[0]))
				.get();
		System.out.println(counts[1]*counts[2]);
		
		System.out.printf("%dms\n", System.currentTimeMillis()-start);
	}

}
