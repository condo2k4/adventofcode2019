package hi.adventofcode;

import hi.utils.math.SoftMath;

import java.math.BigInteger;
import java.util.Arrays;

public class Day12B2 {
	
	public static long gcd(long x, long y) {
		if(x<0) x=-x;
		if(y<0) y=-y;
		if(y==0) return x;
		if(x==1 || y==1) return 1;
		return gcd_internal(y, x%y);
	}
	
	private static long gcd_internal(long x, long y) {
		if(y==0) return x;
		return gcd_internal(y,x%y);
	}
	
	public static long lcm(long a, long b) {
		long gcd = gcd(a, b);
		return (a/gcd)*b;
	}
	
	public static long lcm(long ... vals) {
		switch(vals.length) {
			case 0: throw new IllegalArgumentException();
			case 1: return vals[0];
			case 2: return lcm(vals[0], vals[1]);
			default: {
				long value = lcm(vals[0], vals[1]);
				int index = 2;
				while(index<vals.length)
					value = lcm(value, vals[index++]);
				return value;
			}
		}
	}
	
	private static BigInteger gcd_internal(BigInteger x, BigInteger y) {
		if(y.equals(BigInteger.ZERO)) return x;
		return gcd_internal(y,x.mod(y));
	}
	
	public static BigInteger lcm(BigInteger a, BigInteger b) {
		BigInteger gcd = gcd_internal(a, b);
		return a.divide(gcd).multiply(b);
	}
	
	public static BigInteger biglcm(long ... vals) {
		switch(vals.length) {
			case 0: throw new IllegalArgumentException();
			case 1: return BigInteger.valueOf(vals[0]);
			case 2: return lcm(BigInteger.valueOf(vals[0]), BigInteger.valueOf(vals[1]));
			default: {
				BigInteger value = lcm(BigInteger.valueOf(vals[0]), BigInteger.valueOf(vals[1]));
				int index = 2;
				while(index<vals.length)
					value = lcm(value, BigInteger.valueOf(vals[index++]));
				return value;
			}
		}
	}
	
	public static class OneD {
		private final int initialPos;
		private int pos;
		private int vel;

		public OneD(int initialPos) {
			this.initialPos = initialPos;
			reset();
		}
		
		public final void reset() {
			this.pos = initialPos;
			this.vel = 0;
		}
		
		public void gravitate(OneD[] others) {
			for(OneD a : others) {
				if(a==this) continue;
				gravitate(a);
			}
		}
		
		public void gravitate(OneD other) {
			int acceleration = SoftMath.signum(other.pos-pos);
			vel+=acceleration;
//			other.vel-=acceleration;
		}
		
		public void step() {
			pos+=vel;
		}
		
		public boolean isHome() {
			return vel==0 && pos==initialPos;
		}
	}
	
	public static long[] computeOrbitalPeriods(OneD[] objects) {
		long[] res = new long[objects.length];
		long time = 1;
//		System.out.print(" 0:");
//		for(OneD obj : objects) System.out.printf("\t%3d @ %3d", obj.pos, obj.vel);
//		System.out.println();
		do {
			for(OneD obj : objects) obj.gravitate(objects);
			for(OneD obj : objects) obj.step();
			for(int i=0; i<objects.length; i++) if(res[i]==0 && objects[i].isHome()) res[i]=time;
			time++;
//			System.out.printf("%2d", time);
//			for(OneD obj : objects) System.out.printf("\t%3d @ %3d", obj.pos, obj.vel);
//			System.out.println();
		} while(Arrays.stream(res).anyMatch(x->x==0));
//		System.out.println();
		return res;
	}
	
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		
//		Day12A.Moon[] moons = Day12A.INPUT;
		Day12A.Moon[] moons = SAMPLE2;
		long[] xPeriods = computeOrbitalPeriods(Arrays.stream(moons).map(m -> new OneD(m.getPosX())).toArray(OneD[]::new));
		long[] yPeriods = computeOrbitalPeriods(Arrays.stream(moons).map(m -> new OneD(m.getPosY())).toArray(OneD[]::new));
		long[] zPeriods = computeOrbitalPeriods(Arrays.stream(moons).map(m -> new OneD(m.getPosZ())).toArray(OneD[]::new));
		long xPeriod = lcm(xPeriods);
		long yPeriod = lcm(yPeriods);
		long zPeriod = lcm(zPeriods);
		System.out.printf("%s => %d\n", Arrays.toString(xPeriods), xPeriod);
		System.out.printf("%s => %d\n", Arrays.toString(yPeriods), yPeriod);
		System.out.printf("%s => %d\n", Arrays.toString(zPeriods), zPeriod);
		System.out.println(biglcm(xPeriod, yPeriod, zPeriod));
		
		System.out.printf("%dms\n", System.currentTimeMillis()-start);
	}
	
	public static final Day12A.Moon[] SAMPLE1 = {
		new Day12A.Moon(-1,  0, 2),
		new Day12A.Moon( 2,-10,-7),
		new Day12A.Moon( 4, -8, 8),
		new Day12A.Moon( 3,  5,-1)
	};
	
	public static final Day12A.Moon[] SAMPLE2 = {
		new Day12A.Moon(-8,-10, 0),
		new Day12A.Moon( 5,  5,10),
		new Day12A.Moon( 2, -7, 3),
		new Day12A.Moon( 9, -8,-3)
	};

}