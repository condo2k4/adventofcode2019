package hi.adventofcode;

public class Day2B {
	
	public static final void main(String[] args) {
		long start = System.currentTimeMillis();
outer:	for(int n=0; n<=99; n++)
			for(int v=0; v<=99; v++) {
				long[] program = Day2A.INPUT.clone();
				program[1] = n;
				program[2] = v;
				new Intcode(program).run();
				if(program[0]==19690720) {
					System.out.println(n*100+v);
					break outer;
				}
			}
		System.out.printf("%dms\n", System.currentTimeMillis()-start);
	}

}
