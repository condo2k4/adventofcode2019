package hi.adventofcode;

import java.util.function.BooleanSupplier;

public class SpringScript {
	
	private interface Register {
		boolean read();
		void write(boolean x);
	}
	
	private static class ReadWrite implements Register {
		private boolean value = false;
		@Override public boolean read() { return value; }
		@Override public void write(boolean x) { value=x; }
	}
	
	private static class ReadOnly implements Register {
		private final BooleanSupplier sup;
		public ReadOnly(BooleanSupplier sup) { this.sup = sup; }
		@Override public boolean read() { return sup.getAsBoolean(); }
		@Override public void write(boolean x) { throw new UnsupportedOperationException("Cannot write to read-only."); }
	}
	
	private enum Operator {
		AND, OR, NOT;
		
		public void apply(Register x, Register y) {
			switch(this) {
				case AND: y.write(x.read()&&y.read()); return;
				case OR: y.write(x.read()||y.read()); return;
				case NOT: y.write(!x.read()); return;
				default: throw new IllegalStateException();
			}
		}
	}
	
	private static class Instruction {
		private final Operator operator;
		private final Register x, y;

		public Instruction(Operator operator, Register x, Register y) {
			this.operator = operator;
			this.x = x;
			this.y = y;
		}
		
		public void apply() { operator.apply(x, y); }
	}
	
	private final ReadWrite t=new ReadWrite(), j=new ReadWrite();

	public SpringScript() {
	}

}
