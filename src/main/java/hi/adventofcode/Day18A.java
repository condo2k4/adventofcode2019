package hi.adventofcode;

import hi.utils.CollectionUtils;
import hi.utils.collection.ArrayOrderedSet;
import hi.utils.collection.OrderedSet;
import hi.utils.collection.Singleton;
import hi.utils.tuple.Tuple2;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.stream.Collectors;

public class Day18A {
	
	public static abstract class Tile {
		final int x,y;

		public Tile(int x, int y) {
			this.x = x;
			this.y = y;
		}

		public int getX() { return x; }
		public int getY() { return y; }
		public Character requiredKey() { return null; }
		public boolean isTraversable() { return true; }

		@Override
		public int hashCode() {
			int hash = 7;
			hash = 29 * hash + this.x;
			hash = 29 * hash + this.y;
			return hash;
		}

		@Override
		public boolean equals(Object obj) {
			if(this == obj) return true;
			if(obj == null || getClass() != obj.getClass()) return false;
			final Tile other = (Tile)obj;
			return this.x == other.x && this.y == other.y;
		}
	}
	
	public static class Door extends Tile {
		private final Character key;
		public Door(int x, int y, char key) { super(x,y); this.key = Character.toLowerCase(key); }
		@Override public Character requiredKey() { return key; }
	}
	
	public static class OpenSpace extends Tile {
		public OpenSpace(int x, int y) { super(x, y); }
	}
	
	public static class Key extends OpenSpace {
		private final char key;
		public Key(int x, int y, char key) { super(x,y); this.key = key; }
		@Override public int hashCode() {
			int hash = 7;
			hash = 29 * hash + this.x;
			hash = 29 * hash + this.y;
			hash = 29 * hash + this.key;
			return hash;
		}

		@Override
		public boolean equals(Object obj) {
			if(this == obj) return true;
			if(obj == null || getClass() != obj.getClass()) return false;
			final Key other = (Key)obj;
			return this.x == other.x && this.y == other.y && this.key == other.key;
		}

		@Override public String toString() { return String.valueOf(key); }
	}
	
	public static final Tile WALL = new Tile(-1,-1) {
		@Override public boolean isTraversable() { return false; }
	};
	
	public static class Route {
		final Tile start,  end;
		final Set<Character> requiredKeys;
		final int length;

		public Route(Tile start, Tile end, Set<Character> requiredKeys, int length) {
			this.start = start;
			this.end = end;
			this.requiredKeys = requiredKeys;
			this.length = length;
		}
	}
	
	public static class Map {

		private final Tile start;
		private final Set<Key> keys;
		private final Tile[][] tiles;
		private final int maxX, maxY;

		private Map(Tile start, Set<Key> keys, Tile[][] tiles) {
			this.start = start;
			this.keys = keys;
			this.tiles = tiles;
			maxY = tiles.length-1;
			maxX = tiles[0].length-1;
		}
		
		public Set<Tile> getTraversableNeighbours(Tile t) {
			Set<Tile> set = new ArrayOrderedSet<>(4);
			if(t.x>0) {
				Tile n = tiles[t.y][t.x-1];
				if(n.isTraversable()) set.add(n);
			}
			if(t.y>0) {
				Tile n = tiles[t.y-1][t.x];
				if(n.isTraversable()) set.add(n);
			}
			if(t.x<maxX) {
				Tile n = tiles[t.y][t.x+1];
				if(n.isTraversable()) set.add(n);
			}
			if(t.y<maxY) {
				Tile n = tiles[t.y+1][t.x];
				if(n.isTraversable()) set.add(n);
			}
			return set;
		}

		public Tile getStart() { return start; }
		public Set<Key> getKeys() { return keys; }
		
		public Route path(Tile start, Tile dest) {
			
			class Path implements Comparable<Path>{
				final Tile current;
				final int travel;
				final int cost;
				final Set<Character> requiredKeys;
				public Path(Tile current) { this(current, null, null); }
				public Path(Tile current, Path parent, Character neededKey) {
					this.current = current;
					this.travel = parent==null?0:parent.travel+1;
					if(parent==null || parent.requiredKeys.isEmpty()) {
						requiredKeys = neededKey==null ? Collections.emptySet() : new Singleton<>(neededKey);
					} else {
						if(neededKey==null) {
							requiredKeys = parent.requiredKeys;
						} else {
							requiredKeys = new ArrayOrderedSet<>(parent.requiredKeys.size()+1);
							requiredKeys.addAll(parent.requiredKeys);
							requiredKeys.add(neededKey);
						}
					}
					cost = travel + Math.abs(current.x-dest.x)+Math.abs(current.y-dest.y);
				}
				@Override public int compareTo(Path o) { return Integer.compare(cost, o.cost); }
				public Route toRoute() {
//					System.out.printf("Pathing from (%2d,%2d) to (%2d,%2d) [%c] requires %s\n", start.x, start.y, dest.x, dest.y, ((Key)dest).key, requiredKeys);
					return new Route(start, current, requiredKeys, travel);
				}
			}
			Set<Tile> dejavu = new HashSet<>();
			PriorityQueue<Path> pq = new PriorityQueue<>();
			pq.add(new Path(start));
			while(!pq.isEmpty()) {
				Path p = pq.poll();
				if(p.current.equals(dest)) return p.toRoute();
				if(dejavu.contains(p.current)) continue;
				dejavu.add(p.current);
				for(Tile n : getTraversableNeighbours(p.current))
					pq.add(new Path(n, p, n.requiredKey()));
			}
			return null;
		}

		@Override
		public String toString() {
			return Arrays.stream(tiles)
					.map(row -> {
						return Arrays.stream(row).map(t -> {
							if(t==WALL) return "#";
							if(t instanceof Key) return String.valueOf(((Key)t).key);
							if(t instanceof Door) return String.valueOf(Character.toUpperCase(((Door)t).key));
							return t==start ? "@" : ".";
						}).collect(Collectors.joining());
					}).collect(Collectors.joining("\n"));
		}
	}
	
	public static Map loadMap(String str) {
		String[] lines = str.lines().toArray(String[]::new);
		Tile[][] tiles = new Tile[lines.length][];
		Tile start = null;
		Set<Key> keys = new HashSet<>();
		for(int y=0; y<lines.length; y++) {
			Tile[] row = new Tile[lines[y].length()];
			for(int x=0; x<lines[y].length(); x++) {
				char c = lines[y].charAt(x);
				switch(c) {
					case '#': row[x] = WALL; break;
					case '.': row[x] = new OpenSpace(x, y); break;
					case '@': row[x] = start = new OpenSpace(x, y); break;
					default:
						if(c>='a' && c<='z') {
							Key k = new Key(x,y,(char)c);
							keys.add(k);
							row[x] = k;
						} else if(c>='A' && c<='Z')
							row[x] = new Door(x,y,(char)c);
						else 
							throw new IllegalArgumentException();
				}
			}
			tiles[y]=row;
		}
		return new Map(start, keys, tiles);
	}
		
	public static int path(Map map) {
		java.util.Map<Tuple2<Tile,Tile>, Route> cache = new HashMap<>();
		for(Key k1 : map.keys) {
			for(Key k2 : map.keys) {
				if(k1==k2) continue;
				Route r = map.path(k1, k2);
				cache.put(new Tuple2<>(k1,k2), r);
				cache.put(new Tuple2<>(k2,k1), r);
			}
		}
		int keySum = 0;
		for(Key k : map.keys) {
			Route r = map.path(map.start, k);
			keySum += r.length;
//			if(r.requiredKeys.isEmpty())
				cache.put(new Tuple2<>(map.start,k), r);
		}
		int keyCost= keySum / map.keys.size();
		
		
		class Path implements Comparable<Path>{
			final Tile current;
			final int travel, estimatedCost;
			final OrderedSet<Character> collectedKeys;
			final Set<Key> remainingKeys;
			public Path(Tile current, Set<Key> allKeys) { this(current, 0, CollectionUtils.emptyOrderedSet(), allKeys); }
			public Path(Tile current, int travel, OrderedSet<Character> collectedKeys, Set<Key> remainingKeys) {
				this.current = current;
				this.travel = travel;
				this.collectedKeys = collectedKeys;
				this.remainingKeys = remainingKeys;
//				int remainingEstimate = 0;
//				if(!remainingKeys.isEmpty()) {
//					int c = 0;
//					for(Tile k : remainingKeys) {
//						
//						remainingEstimate += cache.get(new Tuple2<>(current, k)).length;
//					}
//					remainingEstimate/=remainingKeys.size();
//				}
//				estimatedCost = remainingEstimate + travel/(collectedKeys.size()+1);
//				estimatedCost = travel;
//				estimatedCost = travel + remainingKeys.size()*keyCost;
				estimatedCost = travel + remainingKeys.stream().mapToInt(k->cache.get(new Tuple2<>(current,(Tile)k)).length).max().orElse(0);
			}
			@Override public int compareTo(Path o) { return Integer.compare(estimatedCost, o.estimatedCost); }
		}
		Set<Tuple2<Set<Character>, Character>> dejavu = new HashSet<>();
		PriorityQueue<Path> pq = new PriorityQueue<>();
		pq.add(new Path(map.start, map.keys));
		while(!pq.isEmpty()) {
			Path p = pq.poll();
			if(p.remainingKeys.isEmpty()) return p.travel;
//			System.out.printf("Collected: %-34s\tRemaining: %s\n", p.collectedKeys, p.remainingKeys);
			
			if(!p.collectedKeys.isEmpty()) {
				Tuple2<Set<Character>, Character> tpl = new Tuple2<>(p.collectedKeys.subList(0, p.collectedKeys.size()-1), p.collectedKeys.get(p.collectedKeys.size()-1));
				if(dejavu.contains(tpl)) continue;
				dejavu.add(tpl);
			}
			
//			System.out.printf("Using keys: %s\n", p.collectedKeys);
			for(Key k : p.remainingKeys) {
				
				
				Route route = cache.get(new Tuple2<>(p.current, (Tile)k));
//				System.out.printf("Required keys: %s\n", route==null?"[]":route.requiredKeys.toString());
				if(route!=null && p.collectedKeys.containsAll(route.requiredKeys)) {
					OrderedSet<Character> collectedKeys = new ArrayOrderedSet<>(p.collectedKeys.size()+1);
					collectedKeys.addAll(p.collectedKeys);
					collectedKeys.add(k.key);
					Set<Key> remainingKeys = new ArrayOrderedSet<>(p.remainingKeys);
					remainingKeys.remove(k);
					pq.add(new Path(k, p.travel+route.length, collectedKeys, remainingKeys));
				}
			}
		}
		return -1;
	}
	
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		
		Map map = loadMap(INPUT);
		System.out.println(map);
		System.out.println(path(map));
		
		System.out.printf("%dms\n", System.currentTimeMillis()-start);
	}
	
	public static final String SAMPLE1 = "########################\n" +
"#f.D.E.e.C.b.A.@.a.B.c.#\n" +
"######################.#\n" +
"#d.....................#\n" +
"########################";
	
	public static final String SAMPLE2 = "########################\n" +
"#...............b.C.D.f#\n" +
"#.######################\n" +
"#.....@.a.B.c.d.A.e.F.g#\n" +
"########################";
	
	public static final String SAMPLE3 = "#################\n" +
"#i.G..c...e..H.p#\n" +
"########.########\n" +
"#j.A..b...f..D.o#\n" +
"########@########\n" +
"#k.E..a...g..B.n#\n" +
"########.########\n" +
"#l.F..d...h..C.m#\n" +
"#################";
	
	public static final String SAMPLE4 = "########################\n" +
"#@..............ac.GI.b#\n" +
"###d#e#f################\n" +
"###A#B#C################\n" +
"###g#h#i################\n" +
"########################";
	
	public static final String INPUT = "#################################################################################\n" +
"#.#.........#........s..#...#...........#.#z..#.........#...#................e..#\n" +
"#.#.#######.#.###.#####.#.#.#########.#.#.#.#.#.#.#######.#.#.###########.#####.#\n" +
"#...#.....#.#...#.#...#.#.#.....#.....#.#.#.#.#.#.........#....f#.......#.#.....#\n" +
"#.#######.#.#.###.#.###.#.#####.#.#####.#.#.#.#.#########.#######.#####.###.###C#\n" +
"#.#.......#.#.#...#...#...#...#.#...#.#.#...#.R...#.....#.#...#...#.........#...#\n" +
"#.#.#######.#.#.###.#.#####.#.#.###.#.#.#.#####.###.###.#.#.#.#.#.###########.###\n" +
"#.#.#.....#...#.#...#.....#.#.#...#.#...#.#...#.#...#...#...#.#.#.#.......#...#.#\n" +
"#.#.#.###.#.###.#.#####.#.###.###.#.#######.#.###.###.#########.###.#####.#.###.#\n" +
"#...#.#...#.#...#.#...#.#.....#...#.....#...#...#.#.#u........#j..#.#.#...#.....#\n" +
"#.###.#.#####.###.###.#.###.###.###.###.#.#####.#.#.#########.#.#P#.#.#.#######.#\n" +
"#.#...#...#...#.......#.#...#...#.....#.#...#...#x#.....#.....#.#.....#...Y.#...#\n" +
"#.#.#####.#.###.#######.#.###.#########.###.#.###.#####.#.#####.###########.#.###\n" +
"#.#...#.....#...#.....#.#.#.#.#.........#...#.#.....#...#..g..#.#.#.........#...#\n" +
"#####.#######.###.###.#.#.#.#.###.#####.#.###.#####.#.#######.#.#.#.###########.#\n" +
"#...#.#...#.#.#.#...#...#...#...#.#...#.#...#.....#.#.....#...#...#.Q.....#.....#\n" +
"#.#.#.#.#.#.#.#.###.#######.###.#.###.#.#.#.#####.#.#.###.#.#########.###.#.#####\n" +
"#.#.W.#.#.#.#...#...#...#.#.#.#.#...#...#.#.#.......#...#m..........#...#.#.#...#\n" +
"#.#####.#.#.###.#.###.#.#.#.#.#.###.#.###.#.#######################.###.#.#.###.#\n" +
"#.#.....#.#.....#.#...#.#.....#.#...#...#.#.#.......#...#.......#...#...#.#.#...#\n" +
"#.#.#####.#.#####.#.###.#.#####.#.#####.#.#.#.#####.#.#.#.#####.###.#####.#.#.###\n" +
"#k#.#...#.#...V.#...#.#.#...#...#.#...#.#.#...#...#...#...#...#...#.......#.#...#\n" +
"#.#.#.###.#####.#####.#.#####.###.#.#.#.#.#####.#.#########.#####.#########.###.#\n" +
"#.....#.N.#...#...#...#.....#.....#.#...#.#.....#.#...#.....#.....#.....#.......#\n" +
"#######.#.#.#.###.#.#######.#######.#####.###.###.#.#.#.#####.#.###.###.#.#######\n" +
"#.......#.#.#.#.#.#...#...#.........#...#.....#...#.#...#.....#.......#.#...#...#\n" +
"#.###.#####.#.#.#.###.#.#.#######.#####.#######.###.#################.#.#####.#D#\n" +
"#.#.#.#.....#.#.#...#...#.......#.B...#.#.....#...#.#...............#.#..v..#.#.#\n" +
"#.#.#.#.#####.#.###.###########.#####.#.###.#.###.#.#####.#########.#.#####.#.#.#\n" +
"#.#...#.#...#.#.....#.......#...#...#.#.#...#.#...#.#...#.#...#.#...#.....#...#.#\n" +
"#.#.###.###.#.###.#####.#.###.###.###.#.#.###.#.###.#.#.#.#.#.#.#.#############.#\n" +
"#.#...#.#...#...#.#...#.#.#...#...#...#.#.#...#.#...#.#...#.#...#.#.......#...#.#\n" +
"#.#####.#.#####.#.#.#.#.###.###.#.#.###.#.#####.#.###.#####.#####.#.#####.#.#.#.#\n" +
"#.......#.....#.#...#.#.#...#...#.#...#.#.......#.#.....#.......#...#...#...#.#.#\n" +
"#.#########.###.#####.#.#.###.###.###.#.#.#######.###.#.#.#####.#####.#.#####.#.#\n" +
"#...........#...#...#...#...#.#.....#...#.......#...#.#.#.....#...#...#...#...#.#\n" +
"###########.#.###.#.#.#####.#.#.#######.###########.###.#####.###.#.#.#####.###.#\n" +
"#...#...#...#.#...#.#.#.....#.#.#.....#.#...........#...#...#...#...#.#...#.#.I.#\n" +
"#.###.#.#.###.#.###.###.#####.###.#.###.#.###########.###.#####.#####.#.#.#.#.#.#\n" +
"#.....#.....#.....#.....#.........#...................#.............#...#.....#.#\n" +
"#######################################.@.#######################################\n" +
"#...#d..#.....#.....#.......#.....#.#.................#.............A.#.........#\n" +
"#.#.###.#.#.###L###.#.#.###.#.###.#.#.#.#.###########.#.#.#############.###.###.#\n" +
"#.#...#...#...#.#t#.#.#...#a..#...#...#.#.#...#.....#.#.#.....#....l#.....#...#.#\n" +
"#.###.###.###.#.#.#.#.###.#####.#######.#.#.#.#####.#.#.#####.#.###.#.#######.#.#\n" +
"#.#.#o..#.#..i#...#...#.#.#.........#...#...#.......#...#...#.#.#.#...#.#.T.#.#.#\n" +
"#.#.###.#.#.#####.#####.#.#########.#.#.###########.#######.#.#.#.#####.#.#.#.#.#\n" +
"#.#...#.#.#.....#...#...#.#...#.......#.#...#.......#.......#...#.......#.#...#.#\n" +
"#.#H###.#######.###.###.#.#.#.#########.###.#.#######.###.#########.###.#.#####.#\n" +
"#.#.....#.....#.........#...#.....#b....#...#.....#.#.#.#.........#.#...#.....#.#\n" +
"#.###.###.###.###################.###.###.#######.#.#.#.#######.#.#.#########.###\n" +
"#...#.....#.#...#...#...........#...#...#...#.....#.#...#.....#.#...#.......#...#\n" +
"#.#.#######.###.#.#.#.#########.###.#######.#.#####.###.#.#.###.#.###.#####.#.#.#\n" +
"#.#.#...#...#...#.#...#.......#...#.....#...#...#.....#...#.#...#.#...#...#.#.#.#\n" +
"#.#.#.#.#.#.#.###.#####.#########.###.#.#.#####.#.#.#######.#.###.#.###.#.#.###.#\n" +
"#.#.#.#...#.......#.................#.#.#.......#.#.........#...#.#.#.#.#.#...#.#\n" +
"#.#.#.#########.#########.#########.###.#.#########.###########.#.#.#.#.#.###.#.#\n" +
"#.#.#.#.......#.#...#...#.#.......#...#.#.#.......#.#...#.......#.#.#.#.#.....#.#\n" +
"###.#.#.#####.#.#.#.#.#.#.#E#####.###.#.#.#.###.#.#.#.#.#.#########.#.#.#######.#\n" +
"#...#.#.#...#.#.#.#...#.#.#.#.....#...#.#.#.#...#.#...#.#...#.......#.#.#.....#.#\n" +
"#.###.#.#.###.#.#.#####.###.#.#####.###.#.#.#.###.#####.###.#.#######.#.###.#.#.#\n" +
"#.#...#.#...#.#.#.#...#...#.#...#.......#.#.#.#.#.#...#.#...#.#.......#.....#...#\n" +
"#.###.#.#.#.#.#.#Z#.#####.#.###.#######.#.###.#.#.#.#.#.#.#####.###.#############\n" +
"#...#.#.#.#.#.#.#...#.#r..#...#...#...#.#.#...#.#.#.#...#.........#.....#.......#\n" +
"#.#.###.###.#.#.#####.#.#####.###.#.#.#.#.#.###.#.#.###############.###.#.#####.#\n" +
"#.#.#...#...#.#.#.#...#.F.#...#...#.#.#.#.#...#.#.#.#...#.........#...#.#.#...#.#\n" +
"#.#.#.###.#.#.#.#.#.#.###.#.###.###.#.###.###.#.#.###.#.#.#######.###.#.#.#.###.#\n" +
"#.#.#...#.#.#.#...#.#...#...#.#cJ...#...#.....#.#.....#...#.#...#.#...#.#.#...#.#\n" +
"#.#.###.#.#.#.#####.###.#####.#########.#######.###########.#.#.#.#####.#.###.#.#\n" +
"#.#...#.#.#.#.......#.#...#.....#..p..#.#.....#...#.....#.....#.#.....#.#...#.#.#\n" +
"#####.#.#.###########.#.###.#.###.#.###.#.###.#.#.#.###.#.#####.#####.#.###.#K#.#\n" +
"#.....#.#...#.......U.#.....#.#...#.....#.#.#...#...#.#...#...#.#.S...#.......#.#\n" +
"#.#.###.###.#.###.###.#######.#.#########.#.#########.#####.#.#.#.#######.#####.#\n" +
"#.#.#...#...#.#.#.#.#.....#y..#...#.....#...#.........#.....#...#.......#.#...#w#\n" +
"#.###.###.###.#.#.#.#####.#.#####.#.#.#####.#.###.#####.#####.#########.#.#.#.#.#\n" +
"#...#...#.....#.#.#.......#.......#.#...#.#.#...#.#.....#.....#.......#.#n#.#.#.#\n" +
"#.#.###.#.#####.#.#####.###############.#.#.###.#.#.###########.###.###.###.#.#.#\n" +
"#.#...#.#.......#.....#.#.............M.#.#.#...#.#...#.........#...#..h#...#.O.#\n" +
"#.#.###.#############.#G###.###########.#.#.#.###.###.#.#########.###.###.#####.#\n" +
"#.#...............X...#.....#......q....#.....#.....#...........#.........#.....#\n" +
"#################################################################################";

}
