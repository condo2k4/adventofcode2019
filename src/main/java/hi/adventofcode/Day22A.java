package hi.adventofcode;

import java.util.Arrays;
import java.util.function.UnaryOperator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day22A {
	
	public static int[] deck(int length) {
		int[] deck = new int[length];
		while(length-->0) deck[length]=length;
		return deck;
	}
	
	public static int[] stack(int[] deck) {
		int[] newDeck = new int[deck.length];
		for(int i=0, j=deck.length-1; i<deck.length; i++, j--) newDeck[i] = deck[j];
		return newDeck;
	}
	
	public static int[] cut(int[] deck, int position) { return cut_internal(deck, position>0?position:deck.length+position); }
	private static int[] cut_internal(int[] deck, int position) {
		int[] newDeck = new int[deck.length];
		System.arraycopy(deck, 0, newDeck, deck.length-position, position);
		System.arraycopy(deck, position, newDeck, 0, deck.length-position);
		return newDeck;
	}
	
	public static int[] increment(int[] deck, int interval) {
		int[] newDeck = new int[deck.length];
		for(int i=0, j=0; i<deck.length; i++, j=(j+interval)%deck.length) newDeck[j] = deck[i];
		return newDeck;
	}
	
	private static final Pattern PATTERN = Pattern.compile("cut (-?\\d+)|deal into new stack|deal with increment (\\d+)");
	
	public static interface Shuffle extends UnaryOperator<int[]> {
		public static Shuffle parse(String line) {
			Matcher m = PATTERN.matcher(line);
			if(!m.matches()) throw new IllegalArgumentException();
			
			if(m.start(1)>=0) return new Cut(Integer.parseInt(m.group(1)));
			if(m.start(2)>=0) return new Increment(Integer.parseInt(m.group(2)));
			return new Stack();
		}
	}
	
	public static class Stack implements Shuffle {
		@Override public int[] apply(int[] t) { return stack(t); }
	}
	
	public static class Cut implements Shuffle {
		private final int position;
		public Cut(int position) { this.position = position; }
		@Override public int[] apply(int[] t) { return cut(t, position); }
	}
	
	public static class Increment implements Shuffle {
		private final int interval;
		public Increment(int interval) { this.interval = interval; }
		@Override public int[] apply(int[] t) { return increment(t, interval); }
	}
	
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		
//		System.out.println(Arrays.toString(deck(10)));
//		System.out.println(Arrays.toString(stack(deck(10))));
//		System.out.println(Arrays.toString(cut(deck(10),3)));
//		System.out.println(Arrays.toString(cut(deck(10),-4)));
//		System.out.println(Arrays.toString(increment(deck(10),3)));
		
//		System.out.println(Arrays.toString(
//				stack(stack(increment(deck(10), 7)))
//		));
		
		int[] deck = deck(10007);
		for(String line : INPUT.lines().toArray(String[]::new))
			deck = Shuffle.parse(line).apply(deck);
		System.out.println(Arrays.toString(deck));
		for(int i=0; i<deck.length; i++) {
			if(deck[i]==2019) System.out.println(i);
		}
		
		
		System.out.printf("%dms\n", System.currentTimeMillis()-start);
	}
	
	public static final String SAMPLE1 = "deal with increment 7\n" +
"deal into new stack\n" +
"deal into new stack";
	
	public static final String SAMPLE4 = "deal into new stack\n" +
"cut -2\n" +
"deal with increment 7\n" +
"cut 8\n" +
"cut -4\n" +
"deal with increment 7\n" +
"cut 3\n" +
"deal with increment 9\n" +
"deal with increment 3\n" +
"cut -1";
	
	public static final String INPUT = "cut 9002\n" +
"deal with increment 17\n" +
"cut -4844\n" +
"deal with increment 26\n" +
"cut -4847\n" +
"deal with increment 74\n" +
"deal into new stack\n" +
"deal with increment 75\n" +
"deal into new stack\n" +
"deal with increment 64\n" +
"cut 9628\n" +
"deal with increment 41\n" +
"cut 9626\n" +
"deal with increment 40\n" +
"cut -7273\n" +
"deal into new stack\n" +
"deal with increment 20\n" +
"deal into new stack\n" +
"cut 2146\n" +
"deal with increment 7\n" +
"cut -3541\n" +
"deal with increment 25\n" +
"cut -1343\n" +
"deal with increment 42\n" +
"cut -2608\n" +
"deal with increment 75\n" +
"cut -9258\n" +
"deal into new stack\n" +
"cut -2556\n" +
"deal into new stack\n" +
"cut -5363\n" +
"deal into new stack\n" +
"cut -8143\n" +
"deal with increment 15\n" +
"cut -9309\n" +
"deal with increment 65\n" +
"cut -5470\n" +
"deal with increment 9\n" +
"deal into new stack\n" +
"deal with increment 64\n" +
"cut 137\n" +
"deal with increment 40\n" +
"deal into new stack\n" +
"cut 5042\n" +
"deal with increment 74\n" +
"cut 4021\n" +
"deal with increment 39\n" +
"cut -5108\n" +
"deal with increment 50\n" +
"cut -6608\n" +
"deal with increment 64\n" +
"cut 4438\n" +
"deal with increment 48\n" +
"cut 7916\n" +
"deal with increment 23\n" +
"cut -6677\n" +
"deal with increment 27\n" +
"cut -1758\n" +
"deal with increment 32\n" +
"cut -3104\n" +
"deal with increment 37\n" +
"cut 9453\n" +
"deal with increment 20\n" +
"deal into new stack\n" +
"deal with increment 6\n" +
"cut 8168\n" +
"deal with increment 69\n" +
"cut 6704\n" +
"deal with increment 45\n" +
"cut -9423\n" +
"deal with increment 2\n" +
"cut -3498\n" +
"deal with increment 39\n" +
"deal into new stack\n" +
"cut 6051\n" +
"deal with increment 42\n" +
"cut 7140\n" +
"deal into new stack\n" +
"deal with increment 73\n" +
"cut -8201\n" +
"deal into new stack\n" +
"deal with increment 13\n" +
"cut 2737\n" +
"deal with increment 3\n" +
"cut -4651\n" +
"deal into new stack\n" +
"deal with increment 30\n" +
"cut -1505\n" +
"deal with increment 59\n" +
"deal into new stack\n" +
"deal with increment 55\n" +
"cut 8899\n" +
"deal with increment 39\n" +
"cut 8775\n" +
"deal with increment 57\n" +
"cut -1919\n" +
"deal with increment 39\n" +
"cut -3845\n" +
"deal with increment 8\n" +
"cut -4202";

}
