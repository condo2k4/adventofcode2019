package hi.adventofcode;

public class Day9B {
	
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		
		new Intcode(Day9A.INPUT, new Intcode.SequenceSupplier(2), System.out::println).run();
		
		System.out.printf("%dms\n", System.currentTimeMillis()-start);
	}

}
