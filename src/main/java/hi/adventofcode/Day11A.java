package hi.adventofcode;

import java.awt.Point;
import java.util.Arrays;
import java.util.function.LongConsumer;
import java.util.function.LongSupplier;

public class Day11A {
	
	public enum Direction {
		UP, DOWN, LEFT, RIGHT;
		
		private Point apply(Point p) {
			switch(this) {
				case UP: return new Point(p.x, p.y-1);
				case DOWN: return new Point(p.x, p.y+1);
				case LEFT: return new Point(p.x-1, p.y);
				case RIGHT: return new Point(p.x+1, p.y);
				default: throw new UnsupportedOperationException();
			}
		}
		
		private Direction rotate(long direction) {
			switch(this) {
				case UP:    return direction==0 ? LEFT  : RIGHT;
				case DOWN:  return direction==0 ? RIGHT : LEFT;
				case LEFT:  return direction==0 ? DOWN  : UP;
				case RIGHT: return direction==0 ? UP    : DOWN;
				default: throw new UnsupportedOperationException();
			}
		}
	}
	
	public static final int ORIGINAL_BLACK = -1;
	public static final int PAINTED_BLACK  = 0;
	public static final int PAINTED_WHITE  = 1;
	
	public static class RobotState implements LongSupplier, LongConsumer {
		final int[][] hull;
		Direction dir = Direction.UP;
		Point location;
		boolean mode = false;

		public RobotState(int[][] hull, Point initialLocation) {
			this.hull = hull;
			this.location = initialLocation;
		}

		@Override
		public long getAsLong() {
			return hull[location.x][location.y]==PAINTED_WHITE?PAINTED_WHITE:PAINTED_BLACK;
		}

		@Override
		public void accept(long value) {
//			System.out.print(value);
			if(mode) {
				//move
				dir = dir.rotate(value);
				location = dir.apply(location);
//				System.out.println();
			} else {
				//paint
				hull[location.x][location.y] = (int)value;
//				System.out.print(",");
			}
			mode = !mode;
		}
		
	}
	
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		
		int width = 201, height = 201;
		
		final int[][] hull = new int[width][height];
		for(int[] bs : hull) Arrays.fill(bs, ORIGINAL_BLACK);
		final RobotState robot = new RobotState(hull, new Point(width/2,height/2));
		
		new Intcode(INPUT, robot, robot).run();
		
		int count = 0;
		for(int[] bs : hull) {
			for(int b : bs) {
//				System.out.print(b==PAINTED_WHITE?'#':'.');
				if(b>=PAINTED_BLACK) count++;
			}
//			System.out.println();
		}
		System.out.println(count);
		
		System.out.printf("%dms\n", System.currentTimeMillis()-start);
	}
	
	public static long[] TEST = {
		104, 1, 104, 0, 104, 0, 104, 0, 104, 1 ,104, 0, 104, 1, 104, 0, 104, 0, 104, 1, 104, 1, 104, 0, 104, 1, 104, 0, 99
	};
	
	public static long[] INPUT = {
		3,8,1005,8,310,1106,0,11,0,0,0,104,1,104,0,3,8,1002,8,-1,10,101,1,10,10,4,10,1008,8,0,10,4,10,1001,8,0,29,1,2,11,10,1,1101,2,10,2,1008,18,10,2,106,3,10,3,8,1002,8,-1,10,1001,10,1,10,
		4,10,1008,8,1,10,4,10,102,1,8,67,2,105,15,10,3,8,1002,8,-1,10,101,1,10,10,4,10,1008,8,0,10,4,10,1001,8,0,93,2,1001,16,10,3,8,102,-1,8,10,1001,10,1,10,4,10,1008,8,1,10,4,10,102,1,8,
		119,3,8,1002,8,-1,10,1001,10,1,10,4,10,1008,8,1,10,4,10,101,0,8,141,2,7,17,10,1,1103,16,10,3,8,1002,8,-1,10,101,1,10,10,4,10,108,0,8,10,4,10,102,1,8,170,3,8,1002,8,-1,10,1001,10,1,
		10,4,10,1008,8,1,10,4,10,1002,8,1,193,1,7,15,10,2,105,13,10,1006,0,92,1006,0,99,3,8,1002,8,-1,10,101,1,10,10,4,10,108,1,8,10,4,10,101,0,8,228,1,3,11,10,1006,0,14,1006,0,71,3,8,1002,
		8,-1,10,101,1,10,10,4,10,1008,8,0,10,4,10,101,0,8,261,2,2,2,10,1006,0,4,3,8,102,-1,8,10,101,1,10,10,4,10,108,0,8,10,4,10,101,0,8,289,101,1,9,9,1007,9,1049,10,1005,10,15,99,109,632,
		104,0,104,1,21101,0,387240009756L,1,21101,327,0,0,1105,1,431,21101,0,387239486208L,1,21102,1,338,0,1106,0,431,3,10,104,0,104,1,3,10,104,0,104,0,3,10,104,0,104,1,3,10,104,0,104,1,3,10,
		104,0,104,0,3,10,104,0,104,1,21102,3224472579L,1,1,21101,0,385,0,1106,0,431,21101,0,206253952003L,1,21102,396,1,0,1105,1,431,3,10,104,0,104,0,3,10,104,0,104,0,21102,709052072296L,1,
		1,21102,419,1,0,1105,1,431,21102,1,709051962212L,1,21102,430,1,0,1106,0,431,99,109,2,21202,-1,1,1,21102,1,40,2,21102,462,1,3,21102,452,1,0,1105,1,495,109,-2,2105,1,0,0,1,0,0,1,109,
		2,3,10,204,-1,1001,457,458,473,4,0,1001,457,1,457,108,4,457,10,1006,10,489,1101,0,0,457,109,-2,2105,1,0,0,109,4,2102,1,-1,494,1207,-3,0,10,1006,10,512,21101,0,0,-3,22101,0,-3,1,21202,-2,1,2,21102,1,1,3,21101,531,0,0,1105,1,536,109,-4,2106,0,0,109,5,1207,-3,1,10,1006,10,559,2207,-4,-2,10,1006,10,559,21202,-4,1,-4,1105,1,627,22102,1,-4,1,21201,-3,-1,2,21202,-2,2,3,21102,1,578,0,1105,1,536,21202,1,1,-4,21102,1,1,-1,2207,-4,-2,10,1006,10,597,21101,0,0,-1,22202,-2,-1,-2,2107,0,-3,10,1006,10,619,21201,-1,0,1,21102,1,619,0,106,0,494,21202,-2,-1,-2,22201,-4,-2,-4,109,-5,2106,0,0};

}
