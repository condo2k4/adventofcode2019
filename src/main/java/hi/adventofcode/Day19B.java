package hi.adventofcode;

import static hi.adventofcode.Day19A.isPulled;

import java.awt.Point;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Day19B {
	
	public static class Line {
		final int y, x1, x2;

		public Line(int y, int x1, int x2) {
			this.y = y;
			this.x1 = x1;
			this.x2 = x2;
		}

		@Override
		public String toString() {
			return "Line{" + "y=" + y + ", x1=" + x1 + ", x2=" + x2 + '}';
		}
	}
	
	public static Line findLine(int y, int estimatedX) {
		int x1, x2;
		if(isPulled(estimatedX, y)==1) {
			x1=estimatedX-1;
			while(isPulled(x1, y)==1)
				x1--;
			x1++;
			x2=estimatedX+1;
		} else {
			x1 = estimatedX+1;
			while(isPulled(x1, y)==0) {
				x1++;
			}
			x2 = x1+1;
		}
		while(isPulled(x2, y)==1)
			x2++;
		return new Line(y, x1, x2-1);
	}
	
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		
		System.out.println(Stream.iterate(findLine(49, 28), l->findLine(l.y+1, l.x1))
				.filter(l->l.x2-l.x1>100)
//				.peek(l->System.out.printf("Testing %d\n", l.y))
				.flatMap(l -> IntStream.rangeClosed(l.x1, l.x2-99).mapToObj(x->new Point(x,l.y)))
				.filter(p->isPulled(p.x, p.y+99)==1)
				.findFirst()
				.map(p -> p.x*10000+p.y)
				.get());
		
		System.out.printf("%dms\n", System.currentTimeMillis()-start);
	}

}
