package hi.adventofcode;

import hi.utils.collection.primitive.HashMapOfLong;
import hi.utils.collection.primitive.MapOfLong;

import java.awt.Point;
import java.util.function.UnaryOperator;

public class Day3B {
	
	private static final Point ORIGIN = new Point(0,0);
	private static final UnaryOperator<Point>
			UP    = p -> new Point(p.x, p.y-1),
			DOWN  = p -> new Point(p.x, p.y+1),
			LEFT  = p -> new Point(p.x-1, p.y),
			RIGHT = p -> new Point(p.x+1, p.y);
	
	public static MapOfLong<Point> buildWire(String[] parts) {
		Point prev = ORIGIN;
		MapOfLong<Point> times = new HashMapOfLong<>();
		int time=0;
		for(String s : parts) {
			int length = Integer.parseInt(s.substring(1));
			UnaryOperator<Point> succ;
			switch(s.charAt(0)) {
				case 'U': succ = UP; break;
				case 'D': succ = DOWN; break;
				case 'L': succ = LEFT; break;
				case 'R': succ = RIGHT; break;
				default: throw new IllegalArgumentException(s);
			}
			while(length-->0) {
				Point next = succ.apply(prev);
				times.putIfAbsent(next, ++time);
				prev = next;
			}
		}
		return times;
	}
	
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		MapOfLong<Point> wire1 = buildWire(Day3A.WIRE1.split(","));
		MapOfLong<Point> wire2 = buildWire(Day3A.WIRE2.split(","));
		wire1.keySet().retainAll(wire2.keySet());
//		wire2.keySet().retainAll(wire1.keySet()); //not necassary
		
		System.out.println(wire1.entrySet().stream().mapToLong(e->e.getValue()+wire2.get(e.getKey())).min().getAsLong());
		System.out.printf("%dms\n", System.currentTimeMillis()-start);
	}

}
