package hi.adventofcode;

import hi.utils.math.Point;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class Day10A {
	
	public static class Asteroid {
		private final int x, y;
		private final Map<Point, Asteroid> visible = new HashMap<>();

		public Asteroid(int x, int y) {
			this.x = x;
			this.y = y;
		}
		
		private void visible(Collection<Asteroid> asteroids) {
			for(Asteroid a : asteroids) visible(a);
		}
		
		private void visible(Asteroid asteroid) {
			if(asteroid==this) return;
			int dx = asteroid.x-x;
			int dy = asteroid.y-y;
			int len2 = dx*dx+dy*dy;
			double len = Math.sqrt(len2);
			double x = dx/len;
			double y = dy/len;
			Point vector = new Point(x,y);
			
//			System.out.println(visible.keySet().stream()
//					.mapToDouble(p->vector.distanceSquared(p))
//					.min());
			
			Asteroid other = null;
			for(Entry<Point, Asteroid> e : visible.entrySet()) {
				if(vector.distanceSquared(e.getKey())<6.5e-3) {
					other = e.getValue();
					break;
				}
			}
			
//			Asteroid other = visible.get(vector);
			if(other!=null) {
				int odx = other.x-this.x;
				int ody = other.y-this.y;
				int olen2 = odx*odx+ody*ody;
				if(len2<olen2)
					visible.put(vector, asteroid);
			} else {
				visible.put(vector, asteroid);
			}
		}
		
		public int countVisibleAsteroid() {
			return visible.size();
		}

		@Override
		public String toString() {
			return "Asteroid{" + "x=" + x + ", y=" + y + ", v=" + visible.size() + '}';
		}
	}
	
	public static List<Asteroid> loadAsteroids() {
		try {
			List<Asteroid> asteroids = new ArrayList<>(200);
			BufferedReader in = new BufferedReader(new StringReader(SAMPLE2));
			int y = 0;
			String line;
			while( (line=in.readLine()) != null ) {
				for(int x = 0; x < line.length(); x++) {
					if(line.charAt(x)=='#')
						asteroids.add(new Asteroid(x, y));
				}
				y++;
			}
			
			for(Asteroid a : asteroids) {
				a.visible(asteroids);
			}
			return asteroids;
		} catch(IOException ex) {
			ex.printStackTrace();
			return Collections.emptyList();
		}
	}
	
	public static void main(String[] args) throws IOException {
		long start = System.currentTimeMillis();
		
		System.out.println(loadAsteroids().stream()
				.max(Comparator.comparingInt(Asteroid::countVisibleAsteroid))
				.get());
//				.mapToInt(Asteroid::countVisibleAsteroid)
//				.max()
//				.getAsInt());
		
		System.out.printf("%dms\n", System.currentTimeMillis()-start);
	}
	
	public static final String INPUT = "###..#########.#####.\n" +
".####.#####..####.#.#\n" +
".###.#.#.#####.##..##\n" +
"##.####.#.###########\n" +
"###...#.####.#.#.####\n" +
"#.##..###.########...\n" +
"#.#######.##.#######.\n" +
".#..#.#..###...####.#\n" +
"#######.##.##.###..##\n" +
"#.#......#....#.#.#..\n" +
"######.###.#.#.##...#\n" +
"####.#...#.#######.#.\n" +
".######.#####.#######\n" +
"##.##.##.#####.##.#.#\n" +
"###.#######..##.#....\n" +
"###.##.##..##.#####.#\n" +
"##.########.#.#.#####\n" +
".##....##..###.#...#.\n" +
"#..#.####.######..###\n" +
"..#.####.############\n" +
"..##...###..#########";
	
	public static final String SAMPLE1 = "......#.#.\n" +
"#..#.#....\n" +
"..#######.\n" +
".#.#.###..\n" +
".#..#.....\n" +
"..#....#.#\n" +
"#..#....#.\n" +
".##.#..###\n" +
"##...#..#.\n" +
".#....####";
	
	public static final String SAMPLE2 = "#.#...#.#.\n" +
".###....#.\n" +
".#....#...\n" +
"##.#.#.#.#\n" +
"....#.#.#.\n" +
".##..###.#\n" +
"..#...##..\n" +
"..##....##\n" +
"......#...\n" +
".####.###.";
	
	public static final String SAMPLE3 = ".#..#..###\n" +
"####.###.#\n" +
"....###.#.\n" +
"..###.##.#\n" +
"##.##.#.#.\n" +
"....###..#\n" +
"..#.#..#.#\n" +
"#..#.#.###\n" +
".##...##.#\n" +
".....#.#..";
	
	public static final String SAMPLE4 = ".#..##.###...#######\n" +
"##.############..##.\n" +
".#.######.########.#\n" +
".###.#######.####.#.\n" +
"#####.##.#.##.###.##\n" +
"..#####..#.#########\n" +
"####################\n" +
"#.####....###.#.#.##\n" +
"##.#################\n" +
"#####.##.###..####..\n" +
"..######..##.#######\n" +
"####.##.####...##..#\n" +
".#####..#.######.###\n" +
"##...#.##########...\n" +
"#.##########.#######\n" +
".####.#.###.###.#.##\n" +
"....##.##.###..#####\n" +
".#.#.###########.###\n" +
"#.#.#.#####.####.###\n" +
"###.##.####.##.#..##";
	
	public static final String SAMPLE5 = ".#..#\n" +
".....\n" +
"#####\n" +
"....#\n" +
"...##";

}
