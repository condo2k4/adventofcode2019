package hi.adventofcode;

import hi.utils.math.SoftMath;

import java.util.Arrays;


public class Day12A {
	
	public static class Moon {
		private int posX, posY, posZ;
		private int velX, velY, velZ;

		public Moon(int posX, int posY, int posZ) {
			this.posX = posX;
			this.posY = posY;
			this.posZ = posZ;
			velX=velY=velZ=0;
		}
		
		public void gravitate(Moon other) {
			int x = SoftMath.signum(other.posX-posX);
			int y = SoftMath.signum(other.posY-posY);
			int z = SoftMath.signum(other.posZ-posZ);
			velX+=x;
			velY+=y;
			velZ+=z;
			other.velX-=x;
			other.velY-=y;
			other.velZ-=z;
		}
		
		public void step() {
			posX+=velX;
			posY+=velY;
			posZ+=velZ;
		}

		public int getPosX() { return posX; }
		public int getPosY() { return posY; }
		public int getPosZ() { return posZ; }
		
		public int energy() {
			int kineticEnergy = Math.abs(posX)+Math.abs(posY)+Math.abs(posZ);
			int potentialEnergy = Math.abs(velX)+Math.abs(velY)+Math.abs(velZ);
			return kineticEnergy*potentialEnergy;
		}

		@Override
		public String toString() {
			return String.format("pos=<x=%2d,y=%2d,z=%2d>, vel=<x=%2d,y=%2d,z=%2d>", posX, posY, posZ, velX, velY, velZ);
		}
		
		public static void stepAllMoons(Moon[] moons) {
			for(int i=0; i<moons.length; i++) for(int j=i+1; j<moons.length; j++) moons[i].gravitate(moons[j]);
			for(Moon moon : moons) moon.step();
		}
		
	}
	
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		
		Moon[] moons = INPUT;
		for(int s=0; s<1000; s++) {
//			System.out.printf("After %d steps:\n", s);
			Moon.stepAllMoons(moons);
//			for(Moon m : moons) System.out.println(m);
//			System.out.println();
		}
		
		System.out.printf("\nEnery: %d\n", Arrays.stream(moons).mapToInt(Moon::energy).sum());
		
		System.out.printf("%dms\n", System.currentTimeMillis()-start);
	}
	
	public static final Moon[] INPUT = {
		new Moon( 5,13,-3),
		new Moon(18,-7,13),
		new Moon(16, 3, 4),
		new Moon( 0, 8, 8)
	};
	
	private static final Moon[] SAMPLE = {
		new Moon(-1,  0, 2),
		new Moon( 2,-10,-7),
		new Moon( 4, -8, 8),
		new Moon( 3,  5,-1)
	};

}
