package hi.adventofcode;

import hi.utils.assets.vis.FrameStore;
import hi.utils.gui.ComponentFactory;
import hi.utils.gui.components.HIImage;

import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import javax.swing.JFrame;

public class AdventOfCode {
	
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		
		
		
		System.out.printf("%dms\n", System.currentTimeMillis()-start);
	}
	
	public static void showImage(BufferedImage img) {
		JFrame frame = new JFrame("Image Display");
		HIImage before = new HIImage(new FrameStore("Image", img), true, false, true);
		before.setInterpolate(RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR);
		frame.add(ComponentFactory.scrollPane(before));
		frame.setSize(70*25,70*6+50);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	
	public static Stream<String> csvStream(String str, char separator) { return StreamSupport.stream(new StringSpliterator(str, separator), false); }
	public static Iterator<String> csvIter(String str, char separator) { return new StringSpliterator(str, separator); }

    private final static class StringSpliterator implements Spliterator<String>, Iterator<String> {
        private String value;
        private int index;        // current index, modified on advance/split
        private final int fence;  // one past last index
		private final char separator;

        StringSpliterator(String value, char separator) { this(value, 0, value.length(), separator); }
        StringSpliterator(String value, int start, int length, char separator) {
            this.value = value;
            this.index = start;
            this.fence = start + length;
			this.separator = separator;
        }

        private int indexOfLineSeparator(int start) {
            for (int current = start; current < fence; current++) {
                char ch = value.charAt(current);
                if (ch == separator) {
                    return current;
                }
            }
            return fence;
        }

        private int skipLineSeparator(int start) {
            if (start < fence) {
                return start + 1;
            }
            return fence;
        }

		@Override
		public boolean hasNext() {
			return index<fence;
		}

		@Override
        public String next() {
            int start = index;
            int end = indexOfLineSeparator(start);
            index = skipLineSeparator(end);
            return value.substring(start, end);
        }

        @Override
        public boolean tryAdvance(Consumer<? super String> action) {
            if (action == null) {
                throw new NullPointerException("tryAdvance action missing");
            }
            if (index != fence) {
                action.accept(next());
                return true;
            }
            return false;
        }

        @Override
        public void forEachRemaining(Consumer<? super String> action) {
            if (action == null) {
                throw new NullPointerException("forEachRemaining action missing");
            }
            while (index != fence) {
                action.accept(next());
            }
        }

        @Override
        public Spliterator<String> trySplit() {
            int half = (fence + index) >>> 1;
            int mid = skipLineSeparator(indexOfLineSeparator(half));
            if (mid < fence) {
                int start = index;
                index = mid;
                return new StringSpliterator(value, start, mid - start, separator);
            }
            return null;
        }

        @Override
        public long estimateSize() {
            return fence - index + 1;
        }

        @Override
        public int characteristics() {
            return Spliterator.ORDERED | Spliterator.IMMUTABLE | Spliterator.NONNULL;
        }
    }

}
