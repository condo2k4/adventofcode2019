package hi.adventofcode;

import hi.utils.collection.primitive.HashMapOfLong;
import hi.utils.collection.primitive.MapOfLong;
import hi.utils.collection.primitive.MapOfLong.EntryOfLong;

import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Day14A {
	
	private static final Pattern CHEMICAL = Pattern.compile("(\\d+)\\s+(\\w+)");
	
	public static class Reaction {
		
		public static Reaction fromString(String str) {
			String[] split = str.split("=>");
			MapOfLong<String> reactants = new HashMapOfLong<>();
			Matcher m = CHEMICAL.matcher(split[0]);
			while(m.find()) {
				reactants.put(m.group(2), Long.parseLong(m.group(1)));
			}
			m = CHEMICAL.matcher(split[1]);
			if(!m.find()) throw new IllegalArgumentException();
			return new Reaction(m.group(2), Long.parseLong(m.group(1)), reactants);
		}
		
		private final String product;
		private final long productQuantity;
		private final MapOfLong<String> reactants;

		private Reaction(String product, long productQuantity, MapOfLong<String> reactants) {
			this.product = product;
			this.productQuantity = productQuantity;
			this.reactants = reactants;
		}

		public String getProduct() { return product; }
		public long getProductQuantity() { return productQuantity; }
		public MapOfLong<String> getReactants() { return reactants; }
		
		public boolean forwards(MapOfLong<String> chemicals) {
			for(EntryOfLong<String> reactant : reactants.entrySet()) {
				if(chemicals.get(reactant.getKey())<reactant.getValue()) return false;
			}
			
			for(EntryOfLong<String> reactant : reactants.entrySet()) {
				chemicals.adjust(reactant.getKey(), -reactant.getValue());
			}
			chemicals.adjust(product, productQuantity);
			return true;
		}
		
		public boolean backwards(MapOfLong<String> chemicals) {
			if(chemicals.get(product)<=0) return false;
			
			chemicals.adjust(product, -productQuantity);
			chemicals.addAll(reactants);
			return true;
		}
		
	}
	
	private static final Predicate<EntryOfLong<String>> NOT_ORE = e -> !e.getKey().equals("ORE") && e.getValue()>0;
	
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		
		Map<String, Reaction> reactions = INPUT.lines().map(Reaction::fromString).collect(Collectors.toMap(Reaction::getProduct, Function.identity()));
		MapOfLong<String> chemicals = new HashMapOfLong<>();
		chemicals.put("FUEL", 1);
		
		do {
			for(EntryOfLong<String> e : chemicals.entrySet()) {
				if(e.getValue()>0 && !e.getKey().equals("ORE"))
					if(reactions.get(e.getKey()).backwards(chemicals)) break;
			}
		} while(chemicals.entrySet().stream().anyMatch(NOT_ORE));
		
		System.out.println(chemicals.get("ORE"));
		
		System.out.printf("%dms\n", System.currentTimeMillis()-start);
	}
	
	public static String SAMPLE1 = "10 ORE => 10 A\n" +
"1 ORE => 1 B\n" +
"7 A, 1 B => 1 C\n" +
"7 A, 1 C => 1 D\n" +
"7 A, 1 D => 1 E\n" +
"7 A, 1 E => 1 FUEL";
	
	public static String SAMPLE2 = "9 ORE => 2 A\n" +
"8 ORE => 3 B\n" +
"7 ORE => 5 C\n" +
"3 A, 4 B => 1 AB\n" +
"5 B, 7 C => 1 BC\n" +
"4 C, 1 A => 1 CA\n" +
"2 AB, 3 BC, 4 CA => 1 FUEL";
	
	public static String SAMPLE4 = "2 VPVL, 7 FWMGM, 2 CXFTF, 11 MNCFX => 1 STKFG\n" +
"17 NVRVD, 3 JNWZP => 8 VPVL\n" +
"53 STKFG, 6 MNCFX, 46 VJHF, 81 HVMC, 68 CXFTF, 25 GNMV => 1 FUEL\n" +
"22 VJHF, 37 MNCFX => 5 FWMGM\n" +
"139 ORE => 4 NVRVD\n" +
"144 ORE => 7 JNWZP\n" +
"5 MNCFX, 7 RFSQX, 2 FWMGM, 2 VPVL, 19 CXFTF => 3 HVMC\n" +
"5 VJHF, 7 MNCFX, 9 VPVL, 37 CXFTF => 6 GNMV\n" +
"145 ORE => 6 MNCFX\n" +
"1 NVRVD => 8 CXFTF\n" +
"1 VJHF, 6 MNCFX => 4 RFSQX\n" +
"176 ORE => 6 VJHF";
	
	public static String INPUT = "2 LGNW, 1 FKHJ => 3 KCRD\n" +
"5 FVXTS => 5 VSVK\n" +
"1 RBTG => 8 FKHJ\n" +
"2 TLXRM, 1 VWJSD => 8 CDGX\n" +
"1 MVSL, 2 PZDR, 9 CHJRF => 8 CLMZ\n" +
"11 BMSFK => 5 JMSWX\n" +
"10 XRMC => 1 MQLFC\n" +
"20 ZPWQB, 1 SBJTD, 9 LWZXV => 4 JFZNR\n" +
"2 FVXTS => 3 FBHT\n" +
"10 ZPWQB => 8 LGNW\n" +
"5 WBDGL, 16 KZHQ => 2 FVXTS\n" +
"124 ORE => 7 BXFVM\n" +
"5 KCRD => 1 RNVMC\n" +
"5 CGPZC, 4 WJCT, 1 PQXV => 8 VKQXP\n" +
"4 KFVH => 4 FGTKD\n" +
"11 QWQG => 6 LWZXV\n" +
"9 ZMZPB, 8 KFVH, 5 FNPRJ => 3 VKVP\n" +
"1 LFQW, 8 PQXV, 2 TLXRM, 1 VKQXP, 1 BMSFK, 1 QKJPV, 3 JZCFD, 8 VWJSD => 6 WXBC\n" +
"2 SLDWK, 32 JZCFD, 10 RNVMC, 1 FVXTS, 34 LGTX, 1 NTPZK, 1 VKQXP, 1 QTKL => 9 LDZV\n" +
"31 FBHT => 2 BMSFK\n" +
"35 KZHQ, 3 ZPWQB => 3 PCNVM\n" +
"6 DRSG, 1 TDRK, 1 VSVK => 2 VWJSD\n" +
"3 DGMH => 3 ZPWQB\n" +
"162 ORE => 9 RBTG\n" +
"11 LFQW, 1 LPQCK => 8 LGTX\n" +
"8 MQLFC => 1 SBJTD\n" +
"1 KGTB => 9 TGNB\n" +
"1 BXFVM, 1 ZMZPB => 8 FNPRJ\n" +
"1 PCNVM, 15 ZSZBQ => 4 PQXV\n" +
"15 XRMC => 9 ZSZBQ\n" +
"18 VWJSD, 12 CHJRF => 6 KTPH\n" +
"8 RBTG, 5 ZMZPB => 6 KFVH\n" +
"6 SLDWK => 1 XVTRS\n" +
"3 VSVK, 6 BMSFK, 3 NTPZK => 1 JZCFD\n" +
"3 FVXTS, 2 MTMKN => 5 CHJRF\n" +
"9 FNPRJ => 2 QWQG\n" +
"1 FBHT, 1 MVSL, 1 FNPRJ => 1 DRSG\n" +
"35 LPQCK, 19 LWZXV, 28 LGNW => 5 TLXRM\n" +
"5 NKMV => 3 QKJPV\n" +
"3 MGZM, 2 TGNB => 8 PZDR\n" +
"2 FKHJ => 2 WBDGL\n" +
"1 NKMV => 1 KGTB\n" +
"129 ORE => 7 ZMZPB\n" +
"3 LMNQ, 2 BMSFK, 4 RNVMC, 4 KGTB, 4 DRSG, 2 JFZNR, 7 QTKL => 4 CKQZ\n" +
"1 MQLFC => 7 MGZM\n" +
"7 SLDWK, 2 KCRD => 4 WJCT\n" +
"1 QKJPV => 4 LPQCK\n" +
"1 JFZNR => 6 TDRK\n" +
"4 CLMZ, 1 LGTX => 9 PMSZG\n" +
"6 QWQG => 8 CGPZC\n" +
"10 QWQG => 6 LMNQ\n" +
"2 PMSZG, 1 VKVP => 3 QTKL\n" +
"2 DGMH => 8 KZHQ\n" +
"14 RBTG => 9 DGMH\n" +
"62 RNVMC, 4 KTPH, 20 XVTRS, 7 JZCFD, 18 CDGX, 13 WXBC, 14 LDZV, 2 CKQZ, 33 FNPRJ => 1 FUEL\n" +
"8 KGTB, 1 JMSWX => 7 NTPZK\n" +
"1 VKVP, 7 DGMH => 7 NKMV\n" +
"4 LPQCK => 5 MVSL\n" +
"6 KGTB => 2 LFQW\n" +
"2 FGTKD => 9 SLDWK\n" +
"1 WBDGL, 1 ZMZPB, 1 DGMH => 6 XRMC\n" +
"4 VKVP => 7 MTMKN";

}
