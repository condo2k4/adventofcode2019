package hi.adventofcode;

import hi.adventofcode.Day14A.Reaction;
import hi.utils.collection.primitive.HashMapOfLong;
import hi.utils.collection.primitive.MapOfLong;
import hi.utils.collection.primitive.MapOfLong.EntryOfLong;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Day14B {
	
	public static MapOfLong<String> request(String chemical, long toProduce, MapOfLong<String> excessChemicals, Map<String, Reaction> reactions) {
//		System.out.printf("Requesting %d %s\n", toProduce, chemical);
		if(chemical.equals("ORE")) return null;
		Reaction reaction = reactions.get(chemical);
		long perReaction = reaction.getProductQuantity();
		long repeats = (toProduce+perReaction-1)/perReaction;
		MapOfLong<String> resultantChemicals = new HashMapOfLong<>(excessChemicals);
		for(EntryOfLong<String> reactant : reaction.getReactants().entrySet()) {
			long needed = reactant.getValue()*repeats;
			long produce = needed - excessChemicals.get(reactant.getKey());
			if(produce>0) {
				resultantChemicals = request(reactant.getKey(), produce, resultantChemicals, reactions);
				if(resultantChemicals==null) {
//					System.out.printf("Failed to produce %s, not enough %s\n", chemical, reactant.getKey());
					return null;
				}
			}
			resultantChemicals.adjust(reactant.getKey(), -needed);
		}
//		System.out.printf("Produced %d %s\n", perReaction*repeats, chemical);
		resultantChemicals.adjust(chemical, perReaction*repeats);
		return resultantChemicals;
	}
	
	public static MapOfLong<String> bulkFuel(long bulkAmount, MapOfLong<String> chemicals, Map<String, Reaction> reactions) {
		MapOfLong<String> next;
		while( (next=request("FUEL", bulkAmount, chemicals, reactions))!=null ) {
			chemicals = next;
//			System.out.println(chemicals);
		}
		return chemicals;
	}
	
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		
		Map<String, Reaction> reactions = Day14A.INPUT.lines().map(Reaction::fromString).collect(Collectors.toMap(Reaction::getProduct, Function.identity()));
		MapOfLong<String> chemicals = new HashMapOfLong<>();
//		chemicals.put("ORE", 1000000000000L);
		chemicals.put("ORE", 1000000000000L);
		
		chemicals = bulkFuel(100000000000L, chemicals, reactions);
		chemicals = bulkFuel(10000000000L, chemicals, reactions);
		chemicals = bulkFuel(1000000000L, chemicals, reactions);
		chemicals = bulkFuel(100000000L, chemicals, reactions);
		chemicals = bulkFuel(10000000L, chemicals, reactions);
		chemicals = bulkFuel(1000000L, chemicals, reactions);
		chemicals = bulkFuel(100000L, chemicals, reactions);
		chemicals = bulkFuel(10000L, chemicals, reactions);
		chemicals = bulkFuel(1000L, chemicals, reactions);
		chemicals = bulkFuel(100L, chemicals, reactions);
		chemicals = bulkFuel(10L, chemicals, reactions);
		chemicals = bulkFuel(1L, chemicals, reactions);
		
		System.out.println(chemicals.get("FUEL"));
		
		System.out.printf("%dms\n", System.currentTimeMillis()-start);
	}

}
