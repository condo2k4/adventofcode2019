package hi.adventofcode;


import java.util.Arrays;
import java.util.stream.IntStream;

public class Day4B {
	
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		System.out.println(IntStream.rangeClosed(123257, 647015)
				.mapToObj(Integer::toString)
				.map(String::toCharArray)
				.filter(p1->{
					char[] p2 = (char[])p1.clone();
					Arrays.sort(p2);
					return Arrays.equals(p1, p2);
				})
				.filter(p -> {
					int[] counts = new int[10];
					for(char c : p) counts[c-'0']++;
					for(int c : counts) if(c==2) return true;
					return false;
				})
				.count());
		System.out.printf("%dms\n", System.currentTimeMillis()-start);
	}

}
