package hi.adventofcode;

import hi.utils.collection.primitive.IntMap;

import java.util.Arrays;
import java.util.function.IntSupplier;

public class Day16A {
	
	private static final IntMap<int[]> PATTERNS = new IntMap<>(false);
	
	private static IntSupplier getPattern(int phase) {
		final int[] pattern = PATTERNS.computeIfAbsent(phase+1, p->{
			int[] ptrn = new int[p*4];
			int i=0;
			for(int j=0; j<p; j++) ptrn[i++]=0;
			for(int j=0; j<p; j++) ptrn[i++]=1;
			for(int j=0; j<p; j++) ptrn[i++]=0;
			for(int j=0; j<p; j++) ptrn[i++]=-1;
			return ptrn;
		});
		return new IntSupplier() {
			int index = 1;
			@Override
			public int getAsInt() {
				int value = pattern[index];
				index = (index+1)%pattern.length;
				return value;
			}
		};
	}
	
	public static int[] phase(int[] input) {
		int[] result = new int[input.length];
		for(int phase=0; phase<result.length; phase++) {
			IntSupplier pattern = getPattern(phase);
			int sp = 0;
			for(int j : input)
				sp+=j*pattern.getAsInt();
			result[phase] = Math.abs(sp)%10;
		}
		return result;
	}
	
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		
		int[] input = INPUT;
//		System.out.println(Arrays.toString(input));
		for(int i=0; i<100; i++) {
			input = phase(input);
//			System.out.println(Arrays.toString(input));
		}
		System.out.println(Arrays.toString(Arrays.copyOf(input, 8)));
		
		System.out.printf("%dms\n", System.currentTimeMillis()-start);
	}
	
	public static final int[] SAMPLE1 = {1,2,3,4,5,6,7,8};
	
	public static final int[] SAMPLE2 = {8,0,8,7,1,2,2,4,5,8,5,9,1,4,5,4,6,6,1,
		9,0,8,3,2,1,8,6,4,5,5,9,5};
	
	public static final int[] INPUT = {5,9,7,5,4,8,3,5,3,0,4,2,7,9,0,9,5,7,2,3,
		6,6,7,8,3,0,7,6,4,5,5,9,9,9,4,2,0,7,6,6,8,7,2,3,6,1,5,2,7,3,9,0,7,1,2,3,
		8,3,2,8,4,9,5,2,3,2,8,5,8,9,2,9,6,0,9,9,0,3,9,3,4,9,5,7,6,3,0,6,4,1,7,0,
		3,9,9,3,2,8,7,6,3,9,5,9,5,6,1,7,2,8,5,5,3,1,2,5,2,3,2,7,1,3,6,6,3,0,0,9,
		1,6,1,6,3,9,7,8,9,0,3,5,3,3,1,1,6,0,6,0,5,7,0,4,2,2,3,8,6,3,7,5,4,1,7,4,
		8,3,5,9,4,6,3,8,1,0,2,9,5,4,3,4,5,5,5,8,1,7,1,7,7,7,5,2,8,3,5,8,2,6,3,8,
		0,1,3,1,8,3,2,1,5,3,1,2,8,2,2,0,1,8,3,4,8,8,2,6,7,0,9,0,9,5,3,4,0,9,9,3,
		8,7,6,4,8,3,4,1,8,0,8,4,5,6,6,7,6,9,9,5,7,3,2,5,4,5,4,6,4,6,6,8,2,2,2,4,
		3,0,9,9,8,3,5,1,0,7,8,1,2,0,4,7,3,8,6,6,2,3,2,6,8,2,3,2,8,4,2,0,8,2,4,6,
		0,6,4,9,5,7,5,8,4,4,7,4,6,8,4,1,2,0,4,6,5,2,2,5,0,5,2,3,3,6,3,7,4,8,2,3,
		3,8,2,7,3,8,7,8,8,5,7,3,3,6,5,8,2,1,5,7,2,5,5,9,3,0,1,7,1,5,4,7,1,1,2,9,
		1,4,2,0,2,8,4,6,2,6,8,2,9,8,6,0,4,5,9,9,7,6,1,4,1,8,4,2,0,0,5,0,3,3,0,4,
		7,6,3,9,6,7,3,6,4,0,2,6,4,6,4,0,5,5,6,8,4,7,8,7,1,6,9,5,0,1,8,1,9,2,4,1,
		3,6,1,7,7,7,7,8,9,5,9,5,7,1,5,2,8,1,8,4,1,2,5,3,4,7,0,1,8,6,8,5,7,8,5,7,
		6,7,1,0,1,2,8,6,7,2,8,5,9,5,7,3,6,0,7,5,5,6,4,6,4,4,6,9,9,3,2,7,8,9,0,9,
		8,8,8,6,4,6,7,2,4,9,6,3,1,6,6,6,4,2,0,3,2,2,1,7,3,2,2,7,1,2,3,3,7,9,5,4,
		1,5,7,1,6,3,7,7,1,5,5,2,3,7,1,8,2,4,7,4,1,7,8,3,4,9,6,5,1,5,7,7,8,3,7,0,
		6,6,7,9,3,5,5,7,4,4,3,8,3,1,5,6,9,2,7,6,8,4,9,2,9,5,4,7,1,6,3,3,1,4,3,0,
		0,0,1,0,7,2,2,4,0,9,5,9,2,3,5,7,0,8};

}
